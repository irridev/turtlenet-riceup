-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2019 at 01:07 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hack4rice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart_tbl`
--

CREATE TABLE `chart_tbl` (
  `chart_id` int(11) NOT NULL,
  `chart_name` varchar(100) NOT NULL,
  `chart_order` int(100) NOT NULL,
  `chart_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chart_tbl`
--

INSERT INTO `chart_tbl` (`chart_id`, `chart_name`, `chart_order`, `chart_price`) VALUES
(1, 'Rice Premium', 40, 47),
(2, 'Rice, Special', 35, 49),
(3, 'Rice, Fancy', 43, 55),
(4, 'Rice, NFA', 27, 37);

-- --------------------------------------------------------

--
-- Table structure for table `farmer_tbl`
--

CREATE TABLE `farmer_tbl` (
  `farmer_id` int(11) NOT NULL,
  `farmer_uname` varchar(100) NOT NULL,
  `farmer_password` varchar(100) NOT NULL,
  `farmer_fname` varchar(100) NOT NULL,
  `farmer_lname` varchar(100) NOT NULL,
  `farmer_email` varchar(100) NOT NULL,
  `farmer_connum` varchar(100) NOT NULL,
  `farmer_bday` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmer_tbl`
--

INSERT INTO `farmer_tbl` (`farmer_id`, `farmer_uname`, `farmer_password`, `farmer_fname`, `farmer_lname`, `farmer_email`, `farmer_connum`, `farmer_bday`) VALUES
(1, 'farm_user', 'farm_password', 'Pepito', 'Manalotto', 'PManalotto@gmail.com', '09274865249', 'Feb/7/1999'),
(2, 'farm_user1', '1111', 'aaaaaa', 'aaaaa', '', 'aaaa', 'aaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `market_tbl`
--

CREATE TABLE `market_tbl` (
  `market_id` int(11) NOT NULL,
  `market_productname` varchar(100) NOT NULL,
  `market_productprice` double NOT NULL,
  `market_productstock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart_tbl`
--
ALTER TABLE `chart_tbl`
  ADD PRIMARY KEY (`chart_id`);

--
-- Indexes for table `farmer_tbl`
--
ALTER TABLE `farmer_tbl`
  ADD PRIMARY KEY (`farmer_id`);

--
-- Indexes for table `market_tbl`
--
ALTER TABLE `market_tbl`
  ADD PRIMARY KEY (`market_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart_tbl`
--
ALTER TABLE `chart_tbl`
  MODIFY `chart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `farmer_tbl`
--
ALTER TABLE `farmer_tbl`
  MODIFY `farmer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `market_tbl`
--
ALTER TABLE `market_tbl`
  MODIFY `market_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
