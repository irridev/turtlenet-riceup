  <body>
       
      <main class="mdl-layout__content mdl-color--grey-100 column">
        <div class="demo-card-square mdl-card mdl-shadow--4dp mdl-cell--5-col">
        <div class="mdl-card__title mdl-card--expand">
        <a class="mdl-navigation__link" href="<?=base_url()?>doctor/patientdetails">
       
            <h2 class="mdl-card__title-text">Doctor Details</h2></a>
        </div>
        <div class="mdl-card__supporting-text">
            for more Details about Doctor click here
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
            View Details
            </a>
        </div>
        </div>
       
      
          <div class="demo-cards mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-grid mdl-grid column.side">
            <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop ">
              <div class="mdl-card__title mdl-card--expand mdl-color--teal-300">
                <h2 class="mdl-card__title-text">Clinic Logs</h2>
              </div>
              <div class="mdl-card__supporting-text mdl-color-text--grey-600">
                View here to visit clinic activity
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Read More</a>
              </div>
            </div>
            
        
            <div class="demo-card-image mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand"></div>
            <div class="mdl-card__actions">
              <span class="demo-card-image__filename">Image.jpg</span>
            </div>
          </div>

          <div class="demo-card-event mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand">
              <h4>
                Incoming Appointments<br>
              </h4>
            </div>
  <div class="mdl-card__actions mdl-card--border">
    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
      go to appointments
    </a>
    <div class="mdl-layout-spacer"></div>
    <i class="material-icons">event</i>
  </div>
</div>
            </div>
          </div>
        </div>
      </main>
    </div>
     
  </body>
</html>