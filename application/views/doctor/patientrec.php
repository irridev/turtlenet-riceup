
      <main class="mdl-layout__content mdl-color--grey-100">

      

   <div class="row">
        <div class="col-md-0"></div>
        <div class="col-md-12 pad">
            <div id="bod" class="container">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs nav-menu" data-tabs="tabs">
                        <li class="active">
                           
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id ="appsched">
   <nav class="nav-extended">
   
    
    
    <div class="nav-content">
      <ul class="tabs tabs-transparent">
        <li class="tab"><a href="<?=base_url()?>doctor/patientdetails">Patient Details </a></li>
        <li class="tab"><a class="active" href="<?=base_url()?>doctor/patientrec">Patient Records</a></li>
      
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">Sass</a></li>
    <li><a href="badges.html">Components</a></li>
    <li><a href="collapsible.html">JavaScript</a></li>
  </ul>                  
                        
       <br clear="all" />       
  <div class="col-md-11">
  <div class="table-responsive responsiv-table">

      <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      <div class="table-header">
        <span class="table-title"><h4>Here are your patients</h4></span>
        <div class="actions">
          <a href="#add_users" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
     <tr>
     <td><strong> ID</strong></td>
     <td><strong>First name</strong></td>
     <td><strong>Middle name</strong></td>
     <td><strong>Last name</strong></td>
     <td><strong>Diagnosis</strong></td>
     <td><strong>Height</strong></td>
     <td><strong>Weight</strong></td>
     <td><strong>BP</strong></td>
     <td><strong>Treatment</strong></td>
     <td><strong>Medicine</strong></td>
     <td><strong>Created At</strong></td>
     <td><strong>&nbsp; View</strong></td>
       </tr> 
       </thead> 
    <?php foreach($patients as $pat):?>
    <tbody>
     <tr>
     <td><?=$pat->patient_id?></td> 
     <td><?=$pat->fname;?></td>
     <td><?=$pat->mname;?></td>
     <td><?=$pat->lname;?></td>
     <td><?=$pat->diagname;?></td>
     <td><?=$pat->height;?></td>
     <td><?=$pat->weight;?></td>
     <td><?=$pat->blood_press;?></td>
     <td><?=$pat->treatment;?></td>
     <td><?=$pat->pres_med;?></td>
     <td><?=$pat->reg_at;?></td>
     
  <td>
  
  <a class="waves-effect waves-light btn"><i class="material-icons ">remove_red_eye</i></a>
  </td>
     </tr>     
        <?php endforeach; ?>  
    
      </tbody>
      </table>
      
    </div>
  </div>
</div>
</div>
</div>
<?php
	echo anchor('doctor/gendiag_tbl', 'Generate Patient Record');
	
	echo '<p/>';
?>
</div> 
</div>
     <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>

      <br>
      <br>
       
          <div class="demo-graphs mdl-shadow--3dp mdl-color--white mdl-cell mdl-cell--5-col">
            
          </div>
         
            
            </div>
          </div>
        </div>
        
      </main>
    </div>
  