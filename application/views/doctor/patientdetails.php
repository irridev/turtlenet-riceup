
      
      <main class="mdl-layout__content mdl-color--grey-100">

      

   <div class="row">
        <div class="col-md-0"></div>
        <div class="col-md-12 pad">
            <div id="bod" class="container">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs nav-menu" data-tabs="tabs">
                        <li class="active">
                           
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id ="appsched">
   <nav class="nav-extended">
   
    
    
    <div class="nav-content">   
      <ul class="tabs tabs-transparent" >
        <li class="tab"><a href="<?=base_url()?>doctor/patientdetails"> Patient Details </a></li>
        <li class="tab"><a href="<?=base_url()?>doctor/patientrec">Patient Records</a></li>
      
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">Sass</a></li>
    <li><a href="badges.html">Components</a></li>
    <li><a href="collapsible.html">JavaScript</a></li>
  </ul>                  
   
    
    <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      <div class="table-header">
        <span class="table-title"><h4>Here are your patients</h4></span>
        <div class="actions">
          <a href="#add_users" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
     <tr>
     <td><strong>First name</strong></td>
     <td><strong>Middle name</strong></td>
     <td><strong>Last name</strong></td>
     <td><strong>Age</strong></td>
     <td><strong>Birthday</strong></td>
     <td><strong>Sex</strong></td>
     <td><strong>Contact Number</strong></td>
     <td><strong>Address</strong></td>
     <td><strong>Email</strong></td>
     <td><strong>Date Created</strong></td>
     <td><strong>&nbsp; View </strong></td>
       </tr> 
       </thead> 
    <?php foreach($patients as $pat):?>
    <tbody>
     <tr>
     
     <td><?=$pat->fname;?></td>
     <td><?=$pat->mname;?></td>
     <td><?=$pat->lname;?></td>
     <td><?=$pat->age;?></td>
     <td><?=$pat->bdate;?></td>
     <td><?=$pat->sex;?></td>
     <td><?=$pat->con_num;?></td>
     <td><?=$pat->street;?></td>
     <td><?=$pat->email;?></td>
     <td><?=$pat->reg_at;?></td>
     
  <td>
  
  <a class="waves-effect waves-light btn"><i class="material-icons ">remove_red_eye</i></a>
  </td>
     </tr>     
        <?php endforeach; ?>  
    
      </tbody>
      </table>
      
    </div>
  </div>
</div>
<?php
	echo anchor('doctor/genpat_tbl', 'Generate Patient Report');
	
	echo '<p/>';
?>
</div> 
                    
</h1>ADD PATIENTS HERE:
                            <br clear="all" />
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="pro-title">Patient Details</h4>
                                </div>
                                <div class="col-md-11">
                                    <div class="table-responsive responsiv-table">
               <div class="container">
                    <form id="adPat" method="post" action="<?=base_url()?>doctor/patientdetails">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fname">First Name:</label>
                                        <input type="text" class="form-control" name="fname" id="fname">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="mname">Middle Name:</label>
                                        <input type="text" class="form-control" name="mname" id="mname">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lname">Last Name:</label>
                                        <input type="text" class="form-control" name="lname" id="lname">
                                    </div>
                                </div>
                               <div class="col-md-8"></div>
                                    <div class="form-group col-md-3">
                                        <label class="col-md-10 control-label">Sex</label>  
                                          <div class="col-md-12 inputGroupContainer">
                                             <div class="input-group">
                                                 <div class="custom-control custom-radio">
                                                  <input type="radio" class="custom-control-input" id="defaultUnchecked" name="sex">
                                                  <label class="custom-control-label" for="defaultUnchecked">Male</label>
                                                  </div>

                                                    <!-- Default checked -->
                                                  <div class="custom-control custom-radio">
                                                   <input type="radio" class="custom-control-input" id="defaultChecked" name="sex" checked>
                                                   <label class="custom-control-label" for="defaultChecked">Female</label>
                                               </div>
                                            </div>
                                       </div>
                                </div>

                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Birthday:</label>
                                        <div class="input-group date" id="bdate" data-target-input="nearest">
                                              <!-- <input type="date" class="datepicker"> -->
                                            <input type="date" class="datetimepicker" data-target="#bdate" name="bdate" required>
                                            <div class="input-group-append" data-target="#bdate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="age">Age:</label>
                                        <input type="number" class="form-control" name="age" id="age">
                                    </div>
                                </div>
                            </div>       
                        </div>

                     
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" name="email" id="email">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Contact No:</label>
                                    <input type="text" class="form-control" name="con_num" id="cont">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                                <label>Address:</label>
                                <input type="text" class="form-control" name="street" id="street">
                        </div>
                           
      
                <div class="form-group text-center">
                            <!-- <input type="hidden" name="patient_id" value="<//?=$this->uri->segment(3)?>">
                            <input id="subAddPat" type="submit" class="btn btn-primary" value="Add Patient"> -->
                            <button type="submit"  class="btn waves-effect waves-light" name="subAddPat">Add Patient 
                            <i class="material-icons right">add</i></button>
                        </div>
             
                  
                    </form>
        
                     
              
        </div>
</div>
     
      
       
          <div class="demo-graphs mdl-shadow--3dp mdl-color--white mdl-cell mdl-cell--5-col">
            
          </div>
         
            
            </div>
          </div>
        </div>
        
      </main>
    </div>
    
      