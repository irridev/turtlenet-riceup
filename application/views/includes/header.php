<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Lung Center of the Philippines</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?=base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?=base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
   <!--  Scripts-->
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="<?=base_url()?>assets/js/materialize.js"></script>
  <script src="<?=base_url()?>assets/js/init.js"></script>
</head>
  <script>
  $(document).ready(function() {
    $('select').material_select();
  });
      $(document).ready(function(){
      $('.slider').slider();
    });
  
  $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
(function ($) {
    //document ready
    $(function () {
        $(".dropdown-button").dropdown({ hover: false, belowOrigin: true, constrain_width: false, alignment: 'right' });
    }); // end of document ready
})(jQuery); // end of jQuery name space
document.addEventListener("DOMContentLoaded", function(){
	$('.preloader-background').delay(1700).fadeOut('slow');
	
	$('.preloader-wrapper')
		.delay(1700)
		.fadeOut();
});
  </script>
<body>
    