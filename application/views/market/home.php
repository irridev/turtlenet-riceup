
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">


        <div class="container">
        <h2 style="text-align: center">What's New in the RICE Market</h2>  
        <div class="col s12 m8 offset-m2 l6 offset-l3">
        <div class="card-panel grey lighten-5 z-depth-1">
          <div class="row valign-wrapper">
            <div class="col s4">
              <img src="<?=base_url()?>/assets/images/2.jpg" alt="" class="responsive-img"> <!-- notice the "circle" class -->
            </div>
            <div class="col s10">
              <h3>Rice price going down</h3>
              <span class="black-text">
              After several months of increase, the prices of rice and sugar are slowly going down, following the arrival of imports and the peak of harvest season, agencies of the Department of Agriculture (DA) yesterday noted.

              In its regular update on palay (unhusked rice), rice and corn prices, the Philippine Statistics Authority (PSA) said the average wholesale price of well-milled rice on a weekly basis decreased by 1.24 percent to P45.45 per kilo during the first week of this month.
              </span>
            </div>
          </div>
        </div>
      </div>


        <h3> What's in Demand? <h3>
        <h5>Below are the list of types of rice that are in demand in the market</h5>

        <h4>
        <div class="table-responsive">
          <table class="table table-hover">
           <table id="datatable">
        <thead>
     <tr>
     <td><strong>ID</strong></td>
     <td><strong>Name</strong></td>
     <td><strong>Order</strong></td>
     <td><strong>SRP</strong></td>
     
       </tr> 
       </thead> 
    <?php foreach($rice as $kanin):?>
    <tbody>
     <tr>
     
     <td><?=$kanin->chart_id;?></td>
     <td><?=$kanin->chart_name;?></td>
     <td><?=$kanin->chart_order;?></td>
     <td><?=$kanin->chart_price;?></td>
    
     </tr>     
        <?php endforeach; ?>  
    
      </tbody>
      
          </table>
        </div>  
        </h4>


        <div class="col s12 m8 offset-m2 l6 offset-l3">
        <div class="card-panel grey lighten-5 z-depth-1">
          <div class="row valign-wrapper">
            <div class="col s10">
              <h3>Be RICEponsible</h3>
              <span class="black-text">
              School children from Tabug Elementary School in Batac City, Ilocos Norte pledge not to waste rice after participating in a mobile Rice Science Museum exhibit, March 5. Conducted by PhilRice Batac in celebration of its 20th founding anniversary, the exhibit showcased the traditional and modern ways of growing rice. The children also expressed the importance of rice in the Filipino culture through posters.
              </span>
            </div>
            <div class="col s3">
              <img src="<?=base_url()?>/assets/images/field.jpg" alt="" class="circle   responsive-img"> <!-- notice the "circle" class -->
            </div>
          </div>
        </div>
      </div>

      <div class="col s12 m8 offset-m2 l6 offset-l3">
        <div class="card-panel grey lighten-5 z-depth-1">
          <div class="row valign-wrapper">
            <div class="col s3">
              <img src="<?=base_url()?>/assets/images/hybrid.jpg" alt="" class="responsive-img"> <!-- notice the "circle" class -->
            </div>
            <div class="col s10">
              <h3>Hybrid Rice</h3>
              <span class="black-text">
              M20 (NSIC Rc 204H) is a public hybrid rice bred by Philippine Rice Research Institute and (PhilRice) University of the Philippines Los Baños. M20 yields an average of 6.4 t/ha and can reach as high as 11.7 t/ha. It matures in 111 days after sowing. It is moderately resistant to yellow stem borer, green leafhopper, and brown plant hopper.

              M20 is also part of the PhilRice’s Public Hybrid Rice Seed Systems Project with a tagline “Public Hybrid Rice Mestiso. Mura. Maani. Masarap”, which aims to encourage the use of public hybrid seeds including Mestizo 1 among seed growers and farmers.
              </span>
            </div>
          </div>
        </div>
      </div>



      </div>

      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; E-Learning 2018-2019</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->


