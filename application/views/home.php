<style>
    .slider .slides li img {
    background-size:100% auto;
    background-repeat: no-repeat;
}
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.preloader-background {
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #eee;
	
	position: fixed;
	z-index: 100;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;	
}
</style>
<div class="preloader-background">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
		<div class="circle"></div>
            </div>
            <div class="gap-patch">
		<div class="circle"></div>
            </div>
            <div class="circle-clipper right">
		<div class="circle"></div>
            </div>
	</div>
    </div>
</div>
<ul id='dropdown1' class='dropdown-content'>
        <li><a  href='#' data-activates='dropdown2' data-hover="hover" data alignment="left">Drop Me!</a></li>
        <li><a href="#!">two</a></li>
        <li class="divider"></li>
        <li><a href="#!">three</a></li>
</ul>
<div class="navbar-fixed">
    <nav class="blue darken-2" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo white-text"><img class="responsive-img"  width="10%"  src="<?=base_url()?>/assets/images/logo.png"/></a>
            <ul class="right hide-on-med-and-down ">
                <li><a class="white-text" href="#">Home</a></li>
                <li><a class="white-text" href="#!" data-beloworigin="true" data-activates="dropdown1">About Us<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="white-text" href="#">Transparency</a></li>
                <li><a class="white-text" href="#">Services</a></li>
                <li><a class="white-text" href="#">Physicians</a></li>
                <li><a class="white-text" href="#">Fellowship/Training Programs</a></li>
                <li><a class="white-text" href="#">Contact Us</a></li>
                
            </ul>
            <ul id="nav-mobile" class="sidenav">
                <li><a href="#">Navbar Link</a></li>
            </ul>
          <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
      </nav>
 </div>
<div class="container z-depth-1-half" style="padding: 32px 90px 50px 90px; border: 1px solid #EEE;">
    <div class="slider white">
    <ul class="slides white">
      <li>
          <img class="responsive-img" src="<?=base_url()?>/assets/images/irri_1.jpg"  >
      </li>
      <li>
        <img class="responsive-img" src="<?=base_url()?>/assets/images/irri_2.jpg"  >
      </li>
      <li>
        <img class="responsive-img" src="<?=base_url()?>/assets/images/irri_3.jpg" >
      </li>
      
    </ul>
  </div>
</div>
    <nav class="blue darken-2 container" role="navigation">
        <div class="nav-wrapper brand-logo center">
            <ul class=" hide-on-med-and-down ">
                <li><a class="white-text" href="#">LCP Alumni</a></li>
                <li><a class="white-text" href="#">Support</a></li>
                <li><a class="white-text" href="#">Support Programs</a></li>
                <li><a class="white-text" href="#">PHIC ICD-10</a></li>
                <li><a class="white-text" href="#">Invitation to Bid</a></li>
                <li><a class="white-text" href="#">Job Oppurtunities</a></li>
                <li><a class="white-text" href="#">Feedback</a></li>
                <li><a class="white-text" href="#">Links</a></li>
            </ul>
        </div>
      </nav>
<div class="container" style="overflow-y: scroll; height:800px;">
<center><h3 class="blue-text text-darken-2">Announcements</h3></center>
<?php foreach ($post as $pos):?>
    <div class="z-depth-1 grey lighten-4 row" style="padding: 32px 90px 50px 90px; border: 1px solid #EEE;">
        <div class="row">
            <div>
              <div class="card">
                <div class="card-image col s7">
                    <?php
                        $user_img = !empty($pos->image) ? $pos->image : 'default.png';
                    ?>
                    <img class="responsive-img materialboxed" data-caption="<?= date("M j Y, g:i a", $pos->create_date);?>" src="<?=base_url()?>/uploads/<?=$user_img?>" />
                    <span class="card-title">
                        <?= $pos->category?>
                       <br><h6><?= date("M j Y, g:i a", $pos->create_date);?></h6>
                    </span>
                </div>
              </div>
            </div>   
            <div class="col s5">
                <h3 class="blue-text text-darken-2"><?= $pos->title?></h3>
                <p><?= $pos->context?></p>
            </div> 
        </div> 
    </div>
<br><br>
<?php endforeach; ?>
</div>
