<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Admin</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="<?=base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>    
        
         <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="<?=base_url()?>assets/js/materialize.js"></script>
        <script src="<?=base_url()?>assets/js/init.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <script>
            $(document).ready(function() {
                $('select').material_select();
            });
            $(document).ready(function(){
                $('.materialboxed').materialbox();
            });
    </script>
    <body>
        <nav class="blue darken-2" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="#" class="brand-logo white-text">GOVPH</a>
                <ul class="right hide-on-med-and-down">
                   <li><a class="white-text" href="<?= base_url()?>adminview">Make An Announcement</a></li>
                  <li class="active"><a class="white-text" href="<?= base_url()?>adminview/edit">Edit</a></li>
                  <li><a class="white-text" href="<?= base_url()?>adminview/logout">Logout</a></li>
                </ul>
            <ul id="nav-mobile" class="sidenav">
                <li><a href="#">Navbar Link</a></li>
            </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <br><br><br><br><br><br><br>
        <div class="container">        
          <table class="bordered highlight centered">
        <thead>
          <tr>
              <th>Post Id</th>
              <th>Category</th>
              <th>Title</th>
              <th>Context</th>
              <th>Create Date</th>
              <th>Image</th>
              <th>Update Time</th>
          </tr>
        </thead>
        <tbody>
            <?php
                foreach ($post as $pos):
            ?>
            <tr>
                <td><strong><?= $pos->post_id?></strong></td>
                <td><?= $pos->category?></td>
                <td><?= $pos->title?></td>
                <td><?= $pos->context?></td>
                <td><?= date("M j Y, g:i a", $pos->create_date);?></td>
                <td><img class=" materialboxed responsive-img" src="<?=base_url()?>/uploads/<?=$pos->image?>"/></td> 
                <td>
                    <a href="<?= base_url()?>adminview/edit_account/<?= $pos->post_id?>" class="btn-floating waves-effect waves-light teal"><i class="material-icons">mode_edit</i></a><br>
                    <a id="delBtn" data-id="<?= $pos->post_id ?>"  class="btn-floating  waves-effect waves-light red"><i class="material-icons">delete</i></a>
                </td>
            </tr>
            <script>
    $("#delBtn").click(function () {
        var id = $(this).data('id');

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this post!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "<?= base_url() ?>adminview/delete_account/" + id;
                    }
                });
    });

</script>
            <?php endforeach; ?>
        </tbody>
    </table>
            </div>
   
       
       
    </body>
</html>
