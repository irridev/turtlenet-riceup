
<body>
    <!-- Loader -->
    <div class="preloader-background">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div>
                <div class="gap-patch">
                <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
          </div>
        </div>
    </div>
    <!-- Navbar -->
    <div class="navbar-fixed">
        <nav class="green accent-3" role="navigation">
            <div class="nav-wrapper container">
            <a href="<?=base_url()?>portal/"><img class="responsive-img" width="4%" src="<?=base_url()?>assets/images/portal/Palay1.png"
                style="margin-top:1%; cursor: pointer;"/></a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <a href="#!" class="brand-logo">Rice UP</a>
                <ul class="right hide-on-med-and-down ">
                    <li><a class="white-text" href="<?=base_url()?>portal/">Home</a></li>
                    
                    <li><a class="white-text" href="<?=base_url()?>portal/about">About</a></li>
                    <li><a class="white-text" href="<?=base_url()?>portal/market">Market</a></li>
                    <li><a class="white-text"  href="<?=base_url()?>portal/news">News</a></li>
                    <li><a class="white-text"  href="<?=base_url()?>portal/charts">Charts</a></li>
                    <li><a class="white-text model-trigger" href="<?=base_url()?>portal/login">Log in</a></li>
                   
                     
                   
             
                
            <ul class="side-nav" id="mobile-demo">
                <li><a href="sass.html">Sass</a></li>
                <li><a href="badges.html">Components</a></li>
                <li><a href="collapsible.html">Javascript</a></li>
                <li><a href="mobile.html">Mobile</a></li>
            </ul>
        </nav>
    </div> <!-- ./ end Navbar -->

