<!-- Carousel -->
<div class="container z-depth-1-half" style="border: 1px solid #EEE;">
    <div class="slider white">
        <ul class="slides white">
            <li><img class="responsive-img" src="<?=base_url()?>/assets/images/irri_1.jpg"></li>
            <li><img class="responsive-img" src="<?=base_url()?>/assets/images/irri_2.jpg"></li>
            <li><img class="responsive-img" src="<?=base_url()?>/assets/images/irri_3.png"></li>
         
        </ul>
    </div>
</div> <!-- ./ end Carousel -->

<!-- Small Card -->
<div class="row container">
    <!-- Card 1 -->
    <div class="card small col s3">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" size="30" src="<?=base_url()?>/assets/images/portal/card_1.jpg">
        </div>
        <div class="editLink card-content">
            <span class="card-title activator grey-text text-darken-4"><a href="#">Job Oppurtunities</a></span>
        </div>
    </div>

    <!-- Card 2 -->
    <div class="card small col s3">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator responsive-img" src="<?=base_url()?>/assets/images/portal/card_2.jpg">
        </div>
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">Support Services</span>
        </div>
        <div class="card-reveal">
            <span class="noSpace card-title grey-text text-darken-4"><i class="material-icons right">close</i>Support Services</span>        
           
        </div>
    </div>

    <!-- Card 3 -->
    <div class="card small col s3">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" size="30px" src="<?=base_url()?>/assets/images/portal/card_3.jpg">
        </div>
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">Support Programs</span> 
        </div>
        <div class="card-reveal">
            <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>Support Programs</span>
            <p><a href="#">Asthma Club</a></p>
            <p><a href="#">Aviation Maritime & Travel Medicine</a></p>
            <p><a href="#">Bronchiectasis Support Group</a></p>
            <p><a href="#">COPD Support Group</a></p>
            <p><a href="#">Critical Airway & Interventional Pulmonary</a></p>
            <p><a href="#">Directly Observed Therapy (DOTS)</a></p>
            <p><a href="#">Early Lung Cancer Support</a></p>
            <p><a href="#">Esophagus and Swallowing Center</a></p>
            <p><a href="#">Pain Management & Palliative Care</a></p>
            <p><a href="#">Smoking Cessation</a></p>
            <p><a href="#">Wellness Gym</a></p>
        </div>
    </div>

    <!-- Card 4 -->
    <div class="card small col s3">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="<?=base_url()?>/assets/images/portal/card_4.jpg">
        </div>
        <div class="card-content">
            <br><span class="editLink card-title activator grey-text text-darken-4"><a href="#">Market</a></span>   
        </div>
    </div>
</div> <!-- /. end Small Card -->

<!-- Posts -->

</div>