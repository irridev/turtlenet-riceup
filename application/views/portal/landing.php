
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">


        <div class="container">
        <h2 style="text-align: center">What's New in the RICE Market</h2>  
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="assets/images/background/1.jpg" style="width:100%; height:300px;">
            </div>

            <div class="item">
              <img src="assets/images/background/2.jpg" style="width:100%; height:300px;">
            </div>
          
            <div class="item">
              <img src="assets/images/background/3.jpg" style="width:100%; height:300px;">
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>


        <h2> What's NEW? <h2>
        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>

        <h3>
        <div class="table-responsive">
          <table class="table table-hover">
           <table id="datatable">
        <thead>
     <tr>
     <td><strong>First name</strong></td>
     <td><strong>Middle name</strong></td>
     <td><strong>Last name</strong></td>
     <td><strong>Age</strong></td>
     <td><strong>Birthday</strong></td>
     <td><strong>Sex</strong></td>
     <td><strong>Contact Number</strong></td>
     <td><strong>Address</strong></td>
     <td><strong>Email</strong></td>
     <td><strong>Date Created</strong></td>
     <td><strong>&nbsp; View </strong></td>
       </tr> 
       </thead> 
    <?php foreach($patients as $pat):?>
    <tbody>
     <tr>
     
     <td><?=$pat->fname;?></td>
     <td><?=$pat->mname;?></td>
     <td><?=$pat->lname;?></td>
     <td><?=$pat->age;?></td>
     <td><?=$pat->bdate;?></td>
     <td><?=$pat->sex;?></td>
     <td><?=$pat->con_num;?></td>
     <td><?=$pat->street;?></td>
     <td><?=$pat->email;?></td>
     <td><?=$pat->reg_at;?></td>
     </tr>     
        <?php endforeach; ?>  
    
      </tbody>
      
          </table>
        </div>  
        </h3>

      </div>

      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; E-Learning 2018-2019</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->


