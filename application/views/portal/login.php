<!DOCTYPE html>
<html lang="en">
    <link rel="icon" href="<?=base_url()?>assets/images/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Admin Login</title>
		
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo $this->config->base_url()?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo $this->config->base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <style>
       
        .input-field input:focus + label {
            color: #1976d2 !important;
        }
        .row .input-field input:focus {
            border-bottom: 1px solid #1976d2 !important;
            box-shadow: 0 1px 0 0 #1976d2 !important
        }
        
    </style>
    <main>
        <center>
            <div class="section"></div>
            <h3 class="green-text text-darken-2">Please, Sign in</h3>

            <div class="section"></div>

            <div class="container ">
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 90px 50px 90px; border: 1px solid #EEE;">

            <form class="col s12" method="POST" action="<?=base_url()?>portal/loginportal">
                <div class='row'>
                  <div class='col s12'>
                  </div>
                </div>
                <div class='row'>
                  <div class='input-field col s15'>
                      <i class="material-icons prefix green-text text-darken-2">person_outline</i>
                      <input class='validate' type='text' name='farmer_uname' id='farmer_uname' size="45" autofocus/>
                    <label class="green-text text-darken-2" for='farmer_uname'>Enter your username</label>
                  </div>
                </div>
                <div class='row'>
                  <div class='input-field col s12'>
                      <i class="material-icons prefix green-text text-darken-2">lock</i>
                    <input class='validate' type='password' name='farmer_password' id='farmer_password' />
                    <label class="green-text text-darken-2"for='farmer_password'>Enter your password</label>
                  </div>
                </div>
                <br />
                <center>
                  <div class='row'>
                    <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect green darken-2 '>Login</button>
                  </div>
                </center>
            </form>
            </div>
            </div>
            <h5>No account? Register Now</h5>
             <a href="<?=base_url()?>portal/register" name='btn_register' type="button" class='col s12 btn btn-large waves-effect green darken-2 '>Register Now</a>
        </center>
        
  </main>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="<?php echo $this->config->base_url()?>assets/js/materialize.js"></script>
  <script src="<?php echo $this->config->base_url()?>assets/js/init.js"></script>
  </body>
</html>

