<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>

<div class="container">
<br><br><br>
<table>
        <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Specialization</th>
              <th>Clinic Number</th>
              <th>Business Hours</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php 
            $counter = 1;
            foreach ($users as $doc):
        ?>
            <tr>
                <td><?= $counter ?></td>
                <td><?= $doc->fname?> <?= $doc->lname?></td>
                <td><?= $doc->specialty?></td>
                <td><?= $doc->clinic_name?></td>
                <td><?= $doc->doc_start?> - <?= $doc->doc_end?></td>
                <td>
                    <form method="post" action="<?=base_url()?>portal/sample">
                    <input type="hidden" name="doc_id" value=<?= $doc->doc_id?>> 
                    <input type="hidden" name="doc_fname" value=<?= $doc->fname?>>
                    <input type="hidden" name="doc_start1" value=<?= $doc->doc_start?>>
                    <input type="hidden" name="doc_end1" value=<?= $doc->doc_end?>> 
                    <input type="hidden" name="doc_lname" value=<?= $doc->lname?>> 
                    <input type="hidden" name="doc_clinic" value=<?= $doc->clinic_name?> >
                    <button class="btn-floating waves-effect waves-light" type="submit" >
                      <i class="material-icons right">date_range</i>
                    </button>
                    </form>
                </td>
            </tr>
        <?php 
            $counter++;
            endforeach; 
        ?> 
        </tbody>
      </table>
</div>

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</body>
</html>