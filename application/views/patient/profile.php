
<body>
<div class="content">
 <?php foreach($patients as $pat):?>
 <div class="body-01">
    <div class="card-panel blue darken-2">
      <div class="media media--16-9"> <img src="<?php echo base_url('assets/images/admin.png'); ?>" alt="" width="640" height="426"> </div>
      <div class="primary-title">
        <div class="primary-text float-right"><?=$pat->age?></div>
        <div class="primary-text"><?=$pat->fname?><?=$pat->lname?></div>
       
      <div class="supporting-text"><?=$pat->street?></div>
      <div class="supporting-text"><?=$pat->con_num?></div>
      <div class="actions border-top">
        <div class="action-buttons float-right">
          <div class="supporting-text"> <i class="tiny material-icons">email</i><?=$pat->email?></div>
        </div>
         <div class="action-buttons float-right">
          <div class="supporting-text"> <i class="tiny material-icons">cake</i><?=$pat->bdate?></div>
        </div>
        <div class="action-icons"> 
        <i class="material-icons action-icon" role="button" title="Edit">edit</i> 
        <i class="material-icons action-icon" role="button" title="Records">person</i>
        </div>
      </div>
    </div>
    </div>
    <!-- Card example 05 -->
    <?php endforeach;?>
      </main>
    </div>
     
      
  </body>
</html>