
      
      <div class="container">
<br><br><br>
<table>
        <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Specialization</th>
              <th>Clinic Number</th>
              <th>Business Hours</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php 
            $counter = 1;
            foreach ($docs as $doc):
        ?>
            <tr>
                <td><?= $counter ?></td>
                <td><?= $doc->fname?> <?= $doc->lname?></td>
                <td><?= $doc->specialty?></td>
                <td><?= $doc->clinic_num?></td>
                <td><?= $doc->doc_start?> - <?= $doc->doc_end?></td>
                <td>
                    <form method="post" action="<?=base_url()?>portal/sample">
                    <input type="hidden" name="doc_id" value=<?= $doc->doc_id?>> 
                    <input type="hidden" name="doc_fname" value=<?= $doc->fname?>>
                    <input type="hidden" name="doc_start1" value=<?= $doc->doc_start?>>
                    <input type="hidden" name="doc_end1" value=<?= $doc->doc_end?>> 
                    <input type="hidden" name="doc_lname" value=<?= $doc->lname?>> 
                    <input type="hidden" name="doc_clinic" value=<?= $doc->clinic_num?> >
                    <button class="btn-floating waves-effect waves-light" type="submit" >
                      <i class="material-icons right">date_range</i>
                    </button>
                    </form>
                </td>
            </tr>
        <?php 
            $counter++;
            endforeach; 
        ?> 
        </tbody>
      </table>
</div>
     
  </body>
</html>
