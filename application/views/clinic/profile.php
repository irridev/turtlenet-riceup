
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="demo-card-square mdl-card mdl-shadow--4dp mdl-cell--5-col">
        <div class="mdl-card__title mdl-card--expand">
         <div class="container" style="margin-top: 4%;">
    <div class="profile-head">
        <div class="col-sm-4 col-xs-12">
            <img src="img/headLogo.png" class="img-responsive" />
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12">
         <?php foreach($profiles as $profile):?>
            <h5><?=$profile->assistant_name?></h5>
            <ul>
                <li><span class="glyphicon glyphicon-home"></span> <?=$profile->assistant_add?></li>
                <li><span class="glyphicon glyphicon-eye-open"></span>  <?=$profile->assistant_contact?></li>
                <li><span class="glyphicon glyphicon-envelope"></span><?=strtolower($profile->assistant_email)?></li>
            
            </ul>
            <?php endforeach;?>
        </div>
    </div>
</div>
<div id ="bod" class="container">
    <div id="content">
        <ul id="tabs" class="nav nav-tabs nav-menu" data-tabs="tabs">
            <li class="active">
                <a href="#profile" data-toggle="tab">Profile</a>
            </li>
            <li>
                <a href="#editprofile" data-toggle="tab">Edit Profile</a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade active in" id ="profile">
            <div class="container">
                <div class="col-sm-11"style="float:left;">
                    <div class="hve-pro">
                        <p></p>
                    </div>
                </div>
                <br clear="all" />
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="pro-title">About</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive responsiv-table">
                            <table class="table bio-table">
                                <tbody>
                                    
                        
                        <div class="table-responsive responsiv-table">
                            <table class="table bio-table">
                                <tbody>
                                    <tr>
                                       <td>Address</td>
                                       <td>: <?=$profile->assistant_add?></td> 
                                    </tr>
                                    <tr>  
                                       <td>Email Address</td>
                                       <td>: <?=$profile->assistant_email?></td> 
                                    </tr>
                                    <tr>    
                                       <td>Contact Number</td>
                                       <td>: <?$profile->assistant_contact?></td>       
                                    </tr>
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="editprofile">
            <div class="container fom-main">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="register">Edit Your Profile</h2>
                    </div>
                </div>
                <div class="row">
                    <form class="form-horizontal main_form text-left" role="form" method="POST" action="<?=base_url()?>Clinic/editprofile/">
                        <fieldset>
                            <div class="form-group col-md-6">
                                <label class="col-md-10 control-label">Address</label>  
                                <div class="col-md-12 inputGroupContainer">
                                    <div class="input-group">
                                        <input  name="assistant_add" placeholder="Address" class="form-control" value="<?=$profile->assistant_add?>"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-10 control-label">Email Address</label>  
                                <div class="col-md-12 inputGroupContainer">
                                    <div class="input-group">
                                        <input  name="assistant_email" placeholder="Email Address" class="form-control" value="<?=$profile->assistant_email?>"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-10 control-label" >Contact Number</label> 
                                <div class="col-md-12 inputGroupContainer">
                                    <div class="input-group">
                                        <input name="assistant_contact" placeholder="Contact Number" class="form-control" value="<?=$profile->assistant_contact?>" type="text">
                                    </div>
                                </div>
                            </div>
                            
                            </div><br>
                            <div class="form-group">
                                <div class="col-sm-3 col-sm-offset-3">
                                    <button type="reset" class="btn btn-danger btn-block">CLEAR</button>
                                </div>
                                <div class="col-sm-3 ">
                                    <button type="submit" class="btn btn-warning btn-block" name="sreg">SAVE</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>

          <div class="demo-graphs mdl-shadow--3dp mdl-color--white mdl-cell mdl-cell--5-col">
            
          </div>
          <div class="demo-cards mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-grid mdl-grid">
            <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
              <div class="mdl-card__title mdl-card--expand mdl-color--teal-300">
                <h2 class="mdl-card__title-text">Other Contacts</h2>
              </div>
              <div class="mdl-card__supporting-text mdl-color-text--grey-600">
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                    <path fill="#000000" d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
                    </svg><br>
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                    <path fill="#000000" d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z" />
                    </svg><br>
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                    <path fill="#000000" d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />
                    </svg>
              </div>
              <i class="icon-small icon-facebook"></i>
            </div>
            <div class="demo-separator mdl-cell--1-col"></div>
            
          </div>
        </div>
      </main>
    </div>
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" style="position: fixed; left: -1000px; height: -1000px;">
        <defs>
          <mask id="piemask" maskContentUnits="objectBoundingBox">
            <circle cx=0.5 cy=0.5 r=0.49 fill="white" />
            <circle cx=0.5 cy=0.5 r=0.40 fill="black" />
          </mask>
          <g id="piechart">
            <circle cx=0.5 cy=0.5 r=0.5 />
            <path d="M 0.5 0.5 0.5 0 A 0.5 0.5 0 0 1 0.95 0.28 z" stroke="none" fill="rgba(255, 255, 255, 0.75)" />
          </g>
        </defs>
      </svg>
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 500 250" style="position: fixed; left: -1000px; height: -1000px;">
        <defs>
          <g id="chart">
            <g id="Gridlines">
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="27.3" x2="468.3" y2="27.3" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="66.7" x2="468.3" y2="66.7" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="105.3" x2="468.3" y2="105.3" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="144.7" x2="468.3" y2="144.7" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="184.3" x2="468.3" y2="184.3" />
            </g>    
            <g id="Numbers">
              <text transform="matrix(1 0 0 1 485 29.3333)" fill="#888888" font-family="'Roboto'" font-size="9">500</text>
              <text transform="matrix(1 0 0 1 485 69)" fill="#888888" font-family="'Roboto'" font-size="9">400</text>
              <text transform="matrix(1 0 0 1 485 109.3333)" fill="#888888" font-family="'Roboto'" font-size="9">300</text>
              <text transform="matrix(1 0 0 1 485 149)" fill="#888888" font-family="'Roboto'" font-size="9">200</text>
              <text transform="matrix(1 0 0 1 485 188.3333)" fill="#888888" font-family="'Roboto'" font-size="9">100</text>
              <text transform="matrix(1 0 0 1 0 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">1</text>
              <text transform="matrix(1 0 0 1 78 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">2</text>
              <text transform="matrix(1 0 0 1 154.6667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">3</text>
              <text transform="matrix(1 0 0 1 232.1667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">4</text>
              <text transform="matrix(1 0 0 1 309 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">5</text>
              <text transform="matrix(1 0 0 1 386.6667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">6</text>
              <text transform="matrix(1 0 0 1 464.3333 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">7</text>
            </g>
            <g id="Layer_5">
              <polygon opacity="0.36" stroke-miterlimit="10" points="0,223.3 48,138.5 154.7,169 211,88.5
              294.5,80.5 380,165.2 437,75.5 469.5,223.3 	"/>
            </g>
            <g id="Layer_4">
              <polygon stroke-miterlimit="10" points="469.3,222.7 1,222.7 48.7,166.7 155.7,188.3 212,132.7
              296.7,128 380.7,184.3 436.7,125 	"/>
            </g>
          </g>
        </defs>
      </svg>
     
  </body>
</html>
