<body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Home</span>
          <div class="mdl-layout-spacer"></div>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
          <br>
          <br>
            <h1>Welcome to My LCP</h1>
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" id="search">
             
            </div>
          </div>
         
         
        </div>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <img src="assets/images/admin.jpg" class="demo-avatar">
          <div class="demo-avatar-dropdown">
            <span>hello@example.com</span>
            <div class="mdl-layout-spacer"></div>

              <span class="visuallyhidden">Accounts</span>
            </button>
           
              <li class="mdl-menu__item">hello@example.com</li>
              <li class="mdl-menu__item">info@example.com</li>
            
            </ul>
          </div>
        </header>
         <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
         <a class="mdl-navigation__link" href="<?=base_url()?>clinic/dashboard"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/profile"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">person</i>Profile</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/appointments"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">queue</i>Appointments</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/cliniclogs"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">perm_media</i>Clinic Logs</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/patientdetails"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">person_add</i>Patient Details</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/doctordetails"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">person_add</i>Doctor Details</a>
          <a class="mdl-navigation__link" href="<?=base_url()?>clinic/logout"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">arrow_back</i>Log out</a>
          
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">help_outline</i><span class="visuallyhidden">Help</span></a>
        </nav>
      </div>