<?php

class Hack4Rice_Model extends CI_Model {

    // UNIVERSAL QUERIES
    
    public function fetch($table, $where=NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        return ($query->num_rows()) ? $query->result() : false;
    }
    function updateitem($data,$id){
        $this->db->where("patient_id",$id);
        $this->db->update('patient_tbl',$data);
    }
    
    public function insertData($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->affected_rows();
    }

    public function updateData($table, $data, $where = NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->update($table, $data);
        return ($query == true) ? true : false;
    }

    public function deleteData($table, $where = NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->delete($table);
    }
    function fetchAllDoc1(){
        $query = $this->db->get("doc_tbl"); // Select * from item_tbl;
        return $query->result();
    }
 function getinfo($table,$where=NULL){  
        if($where !== NULL){
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    function fetchAllAcc(){
        $query = $this->db->get("farmer_tbl");
        return $query->result();
    }
    function getData($id){
           $this->db->where("chart_id",$id); // WHERE item_id = $id
        $query = $this->db->get("chart_tbl"); // Select * from item_tbl;
        return $query->result();
        //  $this->db->select("chart_name", "chart_order");
        // $this->db->from("chart_tbl",$id);
        // $query = $this->db->get();
        // return $query->result();
    }
    public function insert($data){
        $this->db->insert('farmer_tbl',$data);
    }

}