<?php
class Appointment_Model extends CI_Model {
    
    function fetch_all_id($id) {
        $this->db->where("patient_id",$id); 
        $query = $this->db->get("patient_tbl"); 
        return ($query->num_rows() > 0) ? $query->result() : false;
    
        }
    }
    function fetch_all_dr() {
        $this->db->select('dr_id,dr_name,dr_username');
        $this->db->where('is_deleted', (int) 0);
        $q = $this->db->get('doctor_master');
        if ($q->num_rows() >= 1) {
            return $q->result();
        }
    }
    function fetch_all_patient() {
        $this->db->select('patient_id,fname,lname');
        $this->db->where('is_deleted', (int) 0);
        $q = $this->db->get('mylcp_db');
        if ($q->num_rows() >= 1) {
            return $q->result();
        }
    } 
    function book_appointment() {
        $this->load->helper('date');
        $as_id = $this->fetch_staff_id();
        $time = strtotime($this->input->post('app_date'));
        $newformat = date('Y-m-d', $time);
        $data = array("ap_date" => $newformat,
            "ap_time" => $this->input->post('app_time'),
            "dr_id" => $this->input->post('dr_list'),
            "p_id" => $this->input->post('p_list'),
            'as_id' => $as_id,
            "created_on" => date('Y-m-d'),
            "updated_on" => date('Y-m-d')
        );
        $this->db->insert($this->table, $data);
        return 1;
    }

    function chk_available_appointment($dr_id, $app_date) {
        $time = strtotime($app_date);
        $newformat = date('Y-m-d', $time);
        $this->db->select('ap_time');
        $this->db->where('dr_id', $dr_id);
        $this->db->where('ap_date', $newformat);
        $this->db->where('is_cancel', (int) 0);
        $q = $this->db->get('appointment_master');
        if ($q->num_rows() >= 1) {
            return $q->result();
        }
    }
    function cancel_appointment($ap_id,$pid) {
        $data = array('is_cancel' => (int) 1);
        $this->db->where("ap_id", $ap_id);
        $this->db->update($this->table, $data);
        if ($pid) {
            redirect(base_url() . 'patient/view_appointment_record/' . $pid);
        }
        else
        {
           redirect(base_url() . 'appointment'); 
        }

}