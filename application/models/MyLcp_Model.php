<?php

class MyLcp_Model extends CI_Model {

    // UNIVERSAL QUERIES
    
    public function fetch($table, $where=NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        return ($query->num_rows()) ? $query->result() : false;
    }
    function updateitem($data,$id){
        $this->db->where("patient_id",$id);
        $this->db->update('patient_tbl',$data);
    }
    
    public function insertData($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->affected_rows();
    }

    public function updateData($table, $data, $where = NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->update($table, $data);
        return ($query == true) ? true : false;
    }

    public function deleteData($table, $where = NULL) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->delete($table);
    }
    function fetchAllDoc1(){
        $query = $this->db->get("doc_tbl"); // Select * from item_tbl;
        return $query->result();
    }

    // PORTAL HOME QUERIES
    function fetchPostByDate(){
        $where = "end_date > CURDATE()";
        $this->db->where($where); 
        $query = $this->db->get("post_tbl"); // Select * from post_tbl;
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    public function join($keyword){
        $this->db->select('*');
        $this->db->from('doc_tbl');
        $this->db->where("specialty",$keyword);
        $this->db->join('clinic_tbl', 'doc_tbl.clinic_id=clinic_tbl.clinic_id');
        return $this->db->get()->result();
    }
    
    
    function getinfo($table,$where=NULL){  
        if($where !== NULL){
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    function get_post() {
        $this->db->select('*');
        $this->db->order_by('post_id', 'desc');  
        $this->db->from('post_tbl');
        $this->db->limit('4');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchById($id){
        $this->db->where("doc_id",$id); // WHERE item_id = $id
        $query = $this->db->get("clinic_tbl"); // Select * from item_tbl;
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    function fetchDocByClinic($id){
        $this->db->where("clinic_id",$id); // WHERE item_id = $id
        $query = $this->db->get("doc_tbl"); // Select * from item_tbl;
        return $query->result();
    }

    function fetchPostById($id){
        $this->db->where("post_id",$id); // WHERE item_id = $id
        $query = $this->db->get("post_tbl"); // Select * from item_tbl;
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    
    function updatepost($data,$id){
        $this->db->where("post_id",$id);
        $this->db->update('post_tbl',$data);
    }
    
    function deletepost($id){
        $this->db->where('post_id',$id);
        $this->db->delete('post_tbl');
    }

    function fetchAllPost(){
        $query = $this->db->get("post_tbl");
        return $query->result();
    }
    
    public function fetchAllRecord(){
        $query = $this->db->get("patient_tbl");
        return $query->result();
    }
    public function getDocSched(){
        $this->db->select("title", "start", "end","description","doc_id","clinic_id");
        $this->db->from("sched_tbl");
        $query = $this->db->get();
        return $query->result();
    }
    public function getPhysicians($match){
         $this->db->like('fname',$match);
         $this->db->or_like('mname',$match);
         $this->db->or_like('lname',$match);
         $this->db->or_like('specialty',$match);
         $query = $this->db->get('doc_tbl');
         return $query->result();
      
     
    }
    
    // APPOINTMENT QUERIES
    function fetchAll($tbl){
        $query = $this->db->get($tbl);
        return $query->result();
    }
    public function fetchDocClinic($id){
        return $this->db->select('*')->from('clinic_tbl')->where("clinic_id =",$id)->get()->result();
    }
    
    public function getClinicId(){
        return $this->db->select('*')->get("clinic_tbl")->result();
    }

    public function get_events($app_time, $id){
        return $this->db->where("app_time =", $app_time)->where("sched_id =", $id)->get("sched_tbl");
    }


    // public function get_events($start, $end,$id){
    //     return $this->db->where("start >=", $start)->where("end <=", $end)->where("doc_id =",$id)->get("sched_tbl");
    // }
    public function add_event($data){
        $this->db->insert("sched_tbl", $data);
    }
    public function add_docevent($data){
        $this->db->insert("sched_tbl", $data);
    }

    public function update_event($id, $data){
        $this->db->where("sched_id", $id)->update("sched_tbl", $data);
    }

    public function delete_event($id){
        $this->db->where("sched_id", $id)->delete("sched_tbl");
    }

    // ASSISTANT QUERIES
   
    public function fetchDoc($id){
        $this->db->select('CONCAT(clinic_tbl.assistant_name,'.'" "'.',clinic_tbl.assistant_id) AS doc_id, doc_tbl.fname, doc_tbl.lname, doc_tbl.specialty', FALSE); 
        $this->db->from('clinic_tbl');
        $this->db->join('doc_tbl', 'doc_tbl.clinic_id = clinic_tbl.clinic_id');
       // $this->db->join('clinic_tbl', 'clinic_tbl.assistant_id = clinic_tbl.doc_id');
        $this->db->where('clinic_id', $id);
        $this->db->order_by("doc_tbl.fname", "asc");
        $this->db->order_by("doc_tbl.lname", "asc");
        $query = $this->db->get();
        return $query->result();
        // $this->db->select('CONCAT(user_tbl.user_fname,'.'" "'.',user_tbl.user_lname) AS doc_name, doc_tbl.doc_image, doc_tbl.doc_specialty, doc_tbl.user_id', FALSE);
        // $this->db->from('clinic_tbl');
        // $this->db->join('doc_tbl', 'doc_tbl.user_id = clinic_tbl.doc_id');
        // $this->db->join('user_tbl', 'user_tbl.user_id = doc_tbl.user_id');
        // $this->db->where('clinic_tbl.user_id', $id);
        // $this->db->order_by("user_tbl.user_lname", "asc");
        // $this->db->order_by("user_tbl.user_fname", "asc");
        // $query = $this->db->get();
        // return $query->result();
    }

    public function fetchAllDoc(){
        $this->db->select("doc_id","fname","lname","specialty");
        $this->db->from('doc_tbl');
        // $this->db->where('user_type', 2);
        $this->db->order_by("lname", "asc");
        $this->db->order_by("fname", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function deleteDocClinic($user, $where){
        $this->db->where('doc_id', $user);
        $this->db->where('user_id', $where);
        $this->db->delete('clinic_tbl');
    }
    public function fetchasst($clinic_id){
        $this->db->select("clinic_num","clinic_name","assistant_id","assistant_name","assistant_contact","assistant_add","assistant_email");
        $this->db->from('clinic_tbl');
        $this->db->where('clinic_num', $clinic_id);
        $query = $this->db->get();
        return $query->result();
    }
    function getAsstinfo(){
        $query = $this->db->get('clinic_tbl');
        return $query->result();
        // return ($query->num_rows() > 0) ? $query->result() : false;
    }
    function editasstprofile($aid,$data){
        $this->db->where('assistant_id',$aid);
        $this->db->set('clinic_tbl',$data);
    }
    public function fetchpatientappt(){
        // $this->db->select('name','description','sex','specialization','email','assess','contact_num','app_time','date_created');
        // $this->db->from('sched_tbl');
        // $this->db-where('sched_id',$appointments);
        // $query = $this->db-get();
        // return $query->result();
   
        $query = $this->db->get("sched_tbl");
        return $query->result();
    }


    public function fetchPat(){
        return $this->db->get("patient_tbl");
    }

    public function fetchDiagnosis($id){
        return $this->db->where('dia_user', $id)->get("diagnoses_tbl");
    }

    public function fetchLogs($id){
        return $this->db->where("user_id =",$id)->get("logs_tbl");
    }
    function search($keyword){    
        $this->db->like('specialty',$keyword);
        $query = $this->db->get('doc_tbl');
        return $query->result();
    }

    // ADMIN QUERIES
    
    function fetchAllAcc(){
        $query = $this->db->get("user_tbl");
        return $query->result();
    }

    function fetchAllLogs(){
        $query = $this->db->get("user_logs"); // Select * from item_tbl;
        return $query->result();
    }

    //DOC QUERIES
    function getDocinfo(){
        $query = $this->db->get('doc_tbl');
        return $query->result();
        // return ($query->num_rows() > 0) ? $query->result() : false;
    }
      function editdocprofile($id) {
        $this->db->where('doc_id', $id);
        $this->db->set('doc_tbl',$updateData);
               
    }
     //PATIENT QUERIES
    
     
      public function getPatInfo(){
        $query = $this->db->get('patient_tbl');
        return $query->result();
      }

    // GENERATING PDF FILE

    function fetchPatientTbl(){
        $query = $this->db->get("patient_tbl");
        return $query->result();
    }
    function fetchDiagnosisTbl(){
        $this->db->select('*');
        $this->db->from('diagnosis_tbl');
        $this->db->join('patient_tbl', 'diagnosis_tbl.diag_id = patient_tbl.patient_id');
        
        $query = $this->db->get();
        return $query->result(); 
    }


    
}