<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinic extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','session','form_validation'));
        if($this->session->has_userdata('isClinicloggedin') == FALSE){
            redirect('landing'); 
           }
    }

  public function index(){
       $title = array(
           'title' => "Home",
       );
        $dataArr = $this->session->dataadmin;
        $arrMerge = array_merge($title,$dataArr);
        $this->load->view("clinic/includes/header",$arrMerge);
        $this->load->view("clinic/dashboard");
        $this->load->view("clinic/includes/footer");

    } 


    public function dashboard() {
        $id = $this->session->userid; 
        
        $accountDetails = $this->MyLcp_Model->getinfo("assistant_tbl", $id);
       
        $doctor = array(
            "docs" => $this->MyLcp_Model->fetchDocByClinic($accountDetails->clinic_id)
        );
        $this->load->view("clinic/includes/header");
        $this->load->view("clinic/includes/navbar");
        $this->load->view("clinic/dashboard", $doctor);
        $this->load->view("clinic/includes/footer");
       
    }     
   public function profile(){
        $assistant = $this->MyLcp_Model->getAsstinfo();

        $assistant_profile['profiles'] = $assistant;
        // die(print_r($assistant_profile));
        // foreach ($assistant_acc as $assacc){
        //     $dataArr = array(
        //         'assistant_id'      => "$assacc->assistant_id",
        //         'assistant_name'    => "$assacc->assistant_name",
        //         'assistant_contact' => "$assacc->assistant_contact",
        //         'assistant_add'     => "$assacc->assistant_add",
        //         'assistant_email'   => "$assacc->assistant_email"
        //     );
        $this->load->view("clinic/includes/header");
        $this->load->view("clinic/includes/navbar");
        $this->load->view("clinic/profile", $assistant_profile);
        $this->load->view("clinic/includes/footer");
    // $this->load->view("clinic/profile");
    // $assistant = array(
    //     "assist_info" => $this->MyLcp_Model->fetchasst('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
    //     "doctor"      => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
    // );
   

  }

  public function loadprofile(){
    $clinic_uname = $this->input->post("clinic_uname");
    $clinic_password = $this->input->post("clinic_password");
    $data = array(
        'c_uname'    => strtolower($clinic_uname),
        'c_password' => sha1($clinic_password),
    );
    $data2 = array(
        'c_uname'    => strtolower($clinic_uname),
        'c_password' => sha1($clinic_password),
        'status'     => "active"
    );
    // $assistant_acc = $this->MyLcp_Model->getAsstinfo("clinic_tbl", $data);
    // $assistant_status = $this->MyLcp_Model->getAsstinfo("clinic_tbl", $data2);
    if(!$assistant_acc){
        redirect("");
    }else if($assistant_status){
        redirect("");
    }else{

        $assistant = $this->MyLcp_Model->getAsstinfo();

        $assistant_profile['profiles'] = $assistant;
        die(print_r($assistant_profile));
        // foreach ($assistant_acc as $assacc){
        //     $dataArr = array(
        //         'assistant_id'      => "$assacc->assistant_id",
        //         'assistant_name'    => "$assacc->assistant_name",
        //         'assistant_contact' => "$assacc->assistant_contact",
        //         'assistant_add'     => "$assacc->assistant_add",
        //         'assistant_email'   => "$assacc->assistant_email"
        //     );
        $this->load->view("clinic/includes/header");
        $this->load->view("clinic/includes/navbar");
        $this->load->view("clinic/profile", $assistant_profile);
        $this->load->view("clinic/includes/footer");
        }
    }

  
  public function editprofile(){
    $id = $this->uri->segment(3);
    $data = array(
        "assistant_id" => $id,
    );
    $assistant_acc = $this->MyLcp_Model->getAsstinfo('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id')));
    if(!$assistant_acc){
        echo "<script>alert('Error Account not found'); </script>";
    }else{
        $udpateData = array(
            'assistant_name'    => $this->input->post("assistant_name"), //to load only
            'assistant_contact' => $this->input->post("assistant_contact"),
            'assistant_add'     => $this->input->post("assistant_add"),
            'assistant_email'   => $this->input->post("assistant_email"),
        );
        if(!$this->MyLcp_Model->editasstprofile($id,$updateData)){
            "<script> alert('Account Successfully Updated');</script>";
            redirect("clinic/profile");
        }else{
            echo"<script> alert('Error Account not found'); </script>";
        }
    }
    
  }
  public function appointments(){
       $fetchappt = $this->MyLcp_Model->fetchpatientappt();
    
    
      //  $assistant_profile['profiles'] = $assistant;
        $fetch_appointment['appointments'] = $fetchappt;
    
    $this->load->view("clinic/includes/header");
    $this->load->view("clinic/includes/navbar");
    $this->load->view("clinic/appointments",$fetch_appointment);
    $this->load->view("clinic/includes/footer");
    }
  
   public function cliniclogs(){
    $this->load->view("clinic/cliniclogs");
    }
   
   public function patientdetails(){
       $fetchPatient = $this->MyLcp_Model->fetchPat();
       $fetch_patient['patients'] = $fetchPatient;
       $this->load->view("clinic/includes/header");
       $this->load->view("clinic/includes/navbar");
       $this->load->view("clinic/patientdetails",$fetch_patient);
       $this->load->view("clinic/includes/footer");
      $doctor = array(
            "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
            "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
        );
      $title = array(
            'title' => "Add new Patient"
        );      
        
        $this->form_validation->set_rules('fname',"Firstname","required|alpha"); 
        $this->form_validation->set_rules('mname',"Middlename","required|alpha");   
        $this->form_validation->set_rules('lname',"Lastname","required|alpha");  
        $this->form_validation->set_rules('age',"Age","required|numeric");
        $this->form_validation->set_rules('bdate',"Birthdate","required");    
        $this->form_validation->set_rules('street',"Address","required");    

        $this->form_validation->set_message('required',"{field} is required ");
        $this->form_validation->set_message('number',"{field} must be a numeric ");
        $this->form_validation->set_message('min_length',"{field} must be exactly 11 numbers ");
        $this->form_validation->set_message('max_length',"{field} must be exactly 11 numbers ");
        //$finame = $this->input->post('fname');
        //$laname = $this->input->post('lname');
        $sbmt = $this->input->post("subAddPat");
        if (isset($sbmt)){
            date_default_timezone_set("Asia/Manila");
            
             $data = array(
                'fname' => $this->input->post('fname'),
                'mname' => $this->input->post('mname'),
                'lname' => $this->input->post('lname'),
                'sex' => $this->input->post('sex'),
                'bdate' => strtotime($this->input->post('bdate')),
                'age' => $this->input->post('age'),
                'street' => $this->input->post('street'),
                'con_num' => $this->input->post('con_num'),
                'email' => $this->input->post('email'),
                  
            );
            // $data1 = array(
            //     'patient_id '=>$this->session->userdata('patient_id'),
            //     'date' => time(),
            //     'actvty' => "Added a Patient ".$finame ." ".$laname,
            // );
            if(!$this->MyLcp_Model->insertData('patient_tbl',$data)){
                echo"error";
            }else{
               $this->load->view("clinic/includes/header");
               $this->load->view("clinic/includes/navbar");
               $this->load->view("clinic/patientdetails");
               $this->load->view("clinic/includes/footer");
               
              

            }
    }
    }
    
    public function doctordetails(){
       $this->load->view("clinic/includes/header");
       $this->load->view("clinic/includes/navbar");
       $this->load->view("clinic/doctordetails");
       $this->load->view("clinic/includes/footer");
      $doctor = array(
            "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
            "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
        );
    }
    
   public function logout(){
    $this->session->sess_destroy();
      redirect('portal/home');
    }

    function patient() {
        $doctor = array(
            "clinic" => $this->MyLcp_Model->fetch('patient_tbl', array('patient_id'=>$this->session->userdata('patient_id'))),
            "info" => $this->MyLcp_Model->fetch('patient_tbl', array('patient_id'=>$this->uri->segment(3))),
        );
        if(empty($this->uri->segment(3))){
            $this->load->view('clinic/script-p');
            $this->load->view('clinic/dashboard');
           
        } else
            $this->load->view('clinic/script-p');
            $this->load->view('clinic/patientrec');
        }
    

//     public function addPat(){
//          $title = array(
//             'title' => "Add new Patient"
//         );      
        
//         $this->form_validation->set_rules('fname',"Firstname","required|alpha"); 
//         $this->form_validation->set_rules('mname',"Middlename","required|alpha");   
//         $this->form_validation->set_rules('lname',"Lastname","required|alpha");  
//         $this->form_validation->set_rules('age',"Age","required|numeric");
//         $this->form_validation->set_rules('bdate',"Birthdate","required");    
//         $this->form_validation->set_rules('street',"Address","required");    

//         $this->form_validation->set_message('required',"{field} is required ");
//         $this->form_validation->set_message('number',"{field} must be a numeric ");
//         $this->form_validation->set_message('min_length',"{field} must be exactly 11 numbers ");
//         $this->form_validation->set_message('max_length',"{field} must be exactly 11 numbers ");
//         //$finame = $this->input->post('fname');
//         //$laname = $this->input->post('lname');
//         $sbmt = $this->input->post("subAddPat");
//         if (isset($sbmt)){
//             date_default_timezone_set("Asia/Manila");
            
//              $data = array(
//                 'fname' => $this->input->post('fname'),
//                 'mname' => $this->input->post('mname'),
//                 'lname' => $this->input->post('lname'),
//                 'sex' => $this->input->post('sex'),
//                 'bdate' => strtotime($this->input->post('bdate')),
//                 'age' => $this->input->post('age'),
//                 'street' => $this->input->post('street'),
//                 'con_num' => $this->input->post('con_num'),
//                 'email' => $this->input->post('email'),
                  
//             );
//             // $data1 = array(
//             //     'patient_id '=>$this->session->userdata('patient_id'),
//             //     'date' => time(),
//             //     'actvty' => "Added a Patient ".$finame ." ".$laname,
//             // );
//             if(!$this->MyLcp_Model->insertData('patient_tbl',$data)){
//                 echo"error";
//             }else{
//                $this->load->view("clinic/patientdetails");
              

//             }
//     }
// }

    public function addPatDet() {

              $title = array(
            'title' => "Add new Record"
             );      
        
        $this->form_validation->set_rules('diagname',"DiagnosisName","required|alpha"); 
        $this->form_validation->set_rules('height',"Height","required|alpha");   
        $this->form_validation->set_rules('weight',"Weight","required|alpha");  
        $this->form_validation->set_rules('blood_press',"BloodPressure","required|numeric");
        $this->form_validation->set_rules('treatment',"treatment","required");    
        $this->form_validation->set_rules('pres_med',"prescribedmedicine","required");    

        $this->form_validation->set_message('required',"{field} is required ");
        
        //$finame = $this->input->post('fname');
        //$laname = $this->input->post('lname');
        $sbmt1 = $this->input->post("subAddPatDet");
        if (isset($sbmt1)){
            date_default_timezone_set("Asia/Manila");
            
             $data1 = array(
                'diagname' => $this->input->post('diagname'),
                'height' => $this->input->post('height'),
                'weight' => $this->input->post('weight'),
                'blood_press' => $this->input->post('blood_press'),
                'treatment' => ($this->input->post('treatment')),
                'pres_med' => $this->input->post('pres_med'),
               
            );
            // $data1 = array(
            //     'patient_id '=>$this->session->userdata('patient_id'),
            //     'date' => time(),
            //     'actvty' => "Added a Patient ".$finame ." ".$laname,
            // );
            if(!$this->MyLcp_Model->insertData('diagnosis_tbl',$data1)){
                echo"error";
            }else{
              
               $this->load->view("clinic/includes/header");
               $this->load->view("clinic/includes/navbar");
               $this->load->view("clinic/patientrec");
               $this->load->view("clinic/includes/footer");

            }
    }
}

    public function patientrec(){
         $this->load->view("clinic/includes/header");
         $this->load->view("clinic/includes/navbar");
         $this->load->view("clinic/patientrec");
         $this->load->view("clinic/includes/footer");
            $doctor = array(
            "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
            "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
        );
    }
        
        
      

    public function editPat(){
        $this->load->helper(array('form'));
        $this->form_validation->set_rules('fname',"Firstname","required|alpha"); 
        $this->form_validation->set_rules('lname',"Lastname","required|alpha");  
        $this->form_validation->set_rules('age',"Age","required|numeric");
        $this->form_validation->set_rules('bdate',"Birthdate","required");    
        $this->form_validation->set_rules('con_num',"Contact Number","required|numeric|min_length[10]|max_length[10]");
        $this->form_validation->set_rules('street',"Address","required");    

        $this->form_validation->set_message('required',"{field} is required ");
        $this->form_validation->set_message('number',"{field} must be a numeric ");
        $this->form_validation->set_message('min_length',"{field} must be exactly 11 numbers ");
        $this->form_validation->set_message('max_length',"{field} must be exactly 11 numbers ");
        $id = $this->input->post('patient_id');
        if ($this->form_validation->run() == FALSE){
            $msg = "err"; 
            echo $msg;
        } else {
             $data = array(
                'fname' => $this->input->post('fname'),
                'mname' => $this->input->post('mname'),
                'lname' => $this->input->post('lname'),
                'sex' => $this->input->post('sex'),
                'bdate' => $this->input->post('bdate'),
                'age' => $this->input->post('age'),
                'street' => $this->input->post('street'),
                'con_num' => $this->input->post('con_num'),
                'email' => $this->input->post('email'),
                  
            );
            $this->MyLcp_Model->updateData('patient_tbl',$data,array('patient_id' => $this->input->post('where')));
             $msg = "ok"; echo $msg;
        }
    }


    /* function addPat1(){
        $msg = "err";
        $data = array(
            "fname" => $this->input->post('fname'),
            "mname" => $this->input->post('mname'),
            "lname" => $this->input->post('lname'),
            "sex" => $this->input->post('sex'),
            "email" => $this->input->post('email'),
            "cont" => $this->input->post('cont'),
        );

        if(!preg_match('/[A-Za-z\s]+/', $data['fname']) || !preg_match('/[A-Za-z\s]+/', $data['mname']) ||
        !preg_match('/[A-Za-z\s]+/', $data['lname']) || empty($data['fname']) || empty($data['mname']) || empty($data['lname']) ||
        empty($data['sex']) || empty($data['height']) || empty($data['weight']) || empty($data['btype']) || empty($data['bdate']) ||
        empty($data['email']) || empty($data['cont']) || empty($data['street']) || empty($data['city']) || empty($data['state']) ||
        !preg_match('/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/', $data['email'])){ 
            echo $msg; } else { $msg = "no err"; echo $msg; }

        if($msg == "no err"){
            if($this->lcp_model->insertData('patient_tbl',$data)){
                $msg = "ok"; echo $msg;
            } else {$msg = "bad"; echo $msg; }
        }
    } */

     function editPat1(){
        $msg = "err";
        $data = array(
            "fname" => $this->input->post('fname'),
            "mname" => $this->input->post('mname'),
            "lname" => $this->input->post('lname'),
            "age" => $this->input->post('age'),
            "sex" => $this->input->post('sex'),      
            "email" => $this->input->post('email'),
            "con_num" => $this->input->post('con_num'),
            "street" => $this->input->post('street'),
            "bdate" => date("m-d-Y", strtotime($this->input->post("bdate")))
        );

        if(!preg_match('/[A-Za-z\s]+/', $data['fname']) || !preg_match('/[A-Za-z\s]+/', $data['mname']) ||
        !preg_match('/[A-Za-z\s]+/', $data['lname']) || empty($data['fname']) || empty($data['mname']) || empty($data['lname']) ||
        empty($data['sex']) || empty($data['height']) || empty($data['weight']) || empty($data['btype']) || empty($data['bdate']) ||
        empty($data['email']) || empty($data['con_num']) || empty($data['street']) || empty($data['age']) ||
        !preg_match('/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/', $data['email'])){ 
            echo $msg; } else { $msg = "no err"; echo $msg; }

        if($msg == "no err"){
            if($this->MyLcp_Model->updateData('patient_tbl',$data,array('patient_id' => $this->input->post('patient_id')))){
                $msg = "ok"; echo $msg;
            } else {$msg = "bad"; echo $msg; }
        }
    } 

    // public function logout(){
    //     $this->session->session_destroy();
    //     redirect('portal');
    // }

} 
