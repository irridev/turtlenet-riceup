  <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Hack4Rice_Model');
      $this->load->library(array('session'));
    }
    public function index(){
        redirect('portal/home');
    }
    public function home(){
        //$post = array('post' => $this->MyLcp_Model->fetchPostByDate()); 
        $this->load->view("portal/includes/header");
        $this->load->view("portal/includes/navbar");
        $this->load->view("portal/home");
        $this->load->view("portal/includes/footer");    

    }
   
    public function about() {

        $this->load->view("portal/includes/header");
        $this->load->view("portal/includes/navbar");
        $this->load->view("portal/about");
        $this->load->view("portal/includes/footer");  
    }
    public function market() {

        $data = array(
            'accs' => $this->Hack4Rice_Model->fetchAllAcc(),
        );
        $fetchRiceData = $this->Hack4Rice_Model->fetch('chart_tbl');
        $fetch_rice['rice'] = $fetchRiceData;
        $this->load->view("portal/includes/header");
        $this->load->view("portal/includes/navbar");
        $this->load->view("portal/market",$fetch_rice);
        $this->load->view("portal/includes/footer");  
    }
    public function news() {
        $this->load->view("portal/news");
    }
    public function charts(){
        //$data['chart_data'] = $this->Hack4Rice_Model->getData();
        $id = $this->uri->segment(3);
       $data = array(
           'chart_data' => $this->Hack4Rice_Model->getData($id),
       );
        $this->load->view("portal/includes/header");
        $this->load->view("portal/includes/navbar");
        $this->load->view("portal/charts", $data);
        $this->load->view("portal/includes/footer");  
    }
    public function login(){ 
        $this->load->view("portal/includes/header");
        $this->load->view("portal/login");
    }
    public function loginportal(){
        $data= array(
            'farmer_uname' => $this->input->post('farmer_uname'),
            //'farmer_password' => sha1($this->input->post('farmer_password')),
            'farmer_password' => $this->input->post('farmer_password'),
        );
        $accountDetails = $this->Hack4Rice_Model->getinfo("farmer_tbl", $data);
        if (!$accountDetails) {
            echo "<script>alert('No results found');"
            . "window.location='". base_url()."portal'</script>";
        } else {
            $this->session->isFarmerloggedin = TRUE;
            redirect('portal/view'); 
        }
    }

    public function register(){ 
        $this->load->view("portal/includes/header");
        $this->load->view("portal/register");
    }
    public function registerportal(){
        $this->load->library('form_validation');

        //here are the validation entry
        $this->form_validation->set_rules('farmer_uname', 'Username', 'required');
        $this->form_validation->set_rules('farmer_password', 'Password', 'required');
        $this->form_validation->set_rules('farmer_fname', 'First name', 'required');
        $this->form_validation->set_rules('farmer_lname', 'Last name', 'required');
        $this->form_validation->set_rules('farmer_connum', 'Contact Number', 'required');
        $this->form_validation->set_rules('farmer_bday', 'Birt date', 'required');

        if ($this->form_validation->run() == FALSE)
        {
                $this->register();
        }
        else
        {
                echo 'success';
            
                if($this->form_validation->run())
                {
                $data = array(
                'farmer_uname'  => ($this->input->post('farmer_uname')),
                'farmer_password'  => ($this->input->post('farmer_password')),
                'farmer_fname'  => ($this->input->post('farmer_fname')),
                'farmer_lname'  => ($this->input->post('farmer_lname')),
                'farmer_connum' => ($this->input->post('farmer_connum')),
                'farmer_bday' => ($this->input->post('farmer_bday')));
                
                $this->load->model('Hack4Rice_Model');
                $id = $this->Hack4Rice_Model->insert($data);               
  
                redirect('portal/login');
            }
        }
       
    }

    
   public function view(){
        $data = array(
            'accs' => $this->Hack4Rice_Model->fetchAllAcc(),
        );
        $fetchRiceData = $this->Hack4Rice_Model->fetch('chart_tbl');
        $fetch_rice['rice'] = $fetchRiceData;
        $this->load->view("market/includes/header");
        $this->load->view("market/includes/navbar");
        $this->load->view("market/home",$fetch_rice);
        $this->load->view("market/includes/footer");

   }


   
   
}     
