<?php
class Patient extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','session','form_validation'));
        
    //     if($this->session->has_userdata('isloggedin') == FALSE ||
    //     $this->session->userdata('user_type') != 0){
    //         redirect('dashboard');
    // }
}
     public function index() {
        redirect('portal/dashboard');
    }
    public function dashboard(){
        $keyword=$this->input->post('specialization');
        $data = $this->MyLcp_Model->search($keyword);
        $data1 = array(
            'users' => $this->MyLcp_Model->join($keyword)   
        ); 
        $this->load->view("patient/includes/header");
        $this->load->view("patient/includes/navbar");
         $this->load->view('patient/dashboard',$data1);
        $this->load->view("patient/includes/footer");

         $patient = array(
            "patient" => $this->MyLcp_Model->fetchDocClinic('patient_tbl', array('patient_id'=>$this->session->userdata('patient_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
        );
       
       
    }
     public function profile(){
        $patient = $this->MyLcp_Model->getPatInfo();

        $patient_profile['patients'] = $patient;
        $this->load->view("patient/includes/header");
        $this->load->view("patient/includes/navbar");
        $this->load->view("patient/profile", $patient_profile);
        $this->load->view("patient/includes/footer");
     }
     public function appointment() {
        $this->load->view("patient/includes/header");
        $this->load->view("patient/includes/navbar");
        $this->load->view("patient/addapt");
        $this->load->view("patient/includes/footer");  
       
      
    }
     public function addapt(){
        $this->load->view("patient/includes/header");
        $this->load->view("patient/includes/navbar");
        $this->load->view("patient/search");
        $this->load->view("portal/includes/footer");  

    }
    public function search1() {
        $keyword=$this->input->post('specialization');
        $data = $this->MyLcp_Model->search($keyword); 
        
        // $accountDetails = $this->MyLcp_Model->getinfo("doc_tbl", $data); 
        // $accountDetails = $accountDetails[0];
        // $data1 = $accountDetails->doc_id; 
       
        $data1 = array(
            'users' => $this->MyLcp_Model->join($keyword)   
        );
        // $this->load->view('portal/includes/header_search');    
        $this->load->view('portal/search_sub',$data1); 
        // $this->load->view('portal/includes/footer_search'); 
       
      
    }
    
    public function logout(){
        $this->session->session_destroy();
        redirect('portal');
    }

}