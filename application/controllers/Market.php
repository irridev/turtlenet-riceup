<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Market extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Hack4Rice_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','session','form_validation'));
        $this->load->database();
    }
    public function index(){
        redirect('portal/dashboard');
    }
    public function home(){
        $this->load->view("market/includes/header");
        $this->load->view("market/includes/navbar");
        $this->load->view("market/home");
        $this->load->view("market/includes/footer");    

    }
   public function dashboard() {
        $this->load->view("market/dashboard");
    }
    public function sales() {
        $this->load->view("market/sales");
    }
    public function charts(){ 
        $this->load->view("market/charts");
    }
     public function calendar(){ 
        $this->load->view("market/calendar");
    }
    public function rice(){
        $this->load->view("market/rice");

    }
    public function view(){
        $data = array(
            'accs' => $this->Hack4Rice_Model->fetchAllAcc(),
        );
        $fetchRiceData = $this->Hack4Rice_Model->fetch('chart_tbl');
        $fetch_rice['rice'] = $fetchRiceData;
        $this->load->view("market/includes/header");
        $this->load->view("market/includes/navbar");
        $this->load->view("market/home",$fetch_rice);
        $this->load->view("market/includes/footer");

   }
    // public function sample(){ 
       
    //     $data = array(
    //         'doc_id1' =>$this->input->post("doc_id"),
    //         'doc_rest2' =>$this->input->post("doc_rest1"),
    //         'doc_available2' =>$this->input->post("doc_available1"),
    //         'doc_fname1' =>$this->input->post("doc_fname"),
    //         'doc_lname1' =>$this->input->post("doc_lname"),
    //         'clinic_num' =>$this->input->post("doc_clinic"),
    //         'doc_start2' =>$this->input->post("doc_start1"),
    //         'doc_end2' =>$this->input->post("doc_end1"),
    //       );
    //     $this->load->view("portal/sample",$data);
    // }
    // public function sample_sub(){ 
    //     $curr = date('m/d/Y H:i');
    //     $temp_date = $this->input->post("date");
    //     $temp_time = $this->input->post("time");
    //     $selected = $temp_date . ' ' . $temp_time;

    //     $data = array(
    //         'app_date' => $this->input->post("date"),
    //         'app_time' => $this->input->post("time"),
    //         'app_doc_id' => $this->input->post("doc_id2"),
    //     );
    //     $accountDetails = $this->MyLcp_Model->getinfo("appointment_tbl", $data);

    //     if($curr > $selected){
    //         echo "<script>
    //         alert('DateTime Had Passed');
    //         window.location.href='" . base_url() . "portal/addapt';
    //          </script>"; 
    //     }
    //     else if (!$accountDetails){
    //         $data = array(
    //             'date1' =>$this->input->post("date"),
    //             'time1' =>$this->input->post("time"),
    //             'doc_id3' =>$this->input->post("doc_id2"),
    //             'doc_fname3' =>$this->input->post("doc_fname2"),
    //             'doc_lname3' =>$this->input->post("doc_lname2"),
    //             'clinic_num2' =>$this->input->post("clinic_num1"),
    //           );
                
    //           $this->load->view("portal/sample_sub",$data);
    //     }
    //     else{
    //         echo "<script>
    //         alert('Slot had Already Taken');
    //         window.location.href='" . base_url() . "portal/addapt';
    //          </script>"; 
    //     }
      
    // }
    // public function sample_sub_sub(){ 
    //     $email_to = $this->input->post("email");
    //     $temp_fname = $this->input->post("fname");
    //     $temp_lname = $this->input->post("lname");
    //     $name = $temp_fname . ' ' . $temp_lname;
    
    //     $data = array(
    //       'app_date' =>$this->input->post("date"),
    //       'app_time' =>$this->input->post("time"),
    //       'app_doc_id' =>$this->input->post("doc_id4"),
    //       'app_name' => $name,
    //       'app_email' =>$this->input->post("email"),
    //       'app_contact' =>$this->input->post("contact"),
    //       'app_created' => time(),
    //     );
    //     $this->email->from('masterjeko03@gmail.com', 'Lung Center of The Phillippines');
    //     $this->email->to($email_to);
    //     $this->email->subject('Appointment');
    //     $this->email->message($this->load->view('welcome_message',$data,true));
    //     $this->email->send();
    //     $this->MyLcp_Model->insertData("appointment_tbl",$data);
    //     echo "<script>
    //     alert('Successfully Appoint');
    //     window.location.href='" . base_url() . "portal';
    //      </script>"; 
       
    //   }
    // public function services(){
    //     $this->load->view("portal/services");
    // }
    //   public function physicians(){
       
      
    //     $data['physicians'] = array();
    //     if ($query = $this->MyLcp_Model->getPhysicians($this->input->post('search'))){
    //         $data['physicians'] = $query;
    //     }
    //     $this->load->view('portal/physicians');

    // }

    // //appointment module
    //   public function appointment() {
    //     $this->load->view("portal/includes/header");
    //     $this->load->view("portal/includes/navbar");
    //     $this->load->view("portal/addapt");
    //     $this->load->view("portal/includes/footer");  
       
      
    // }
   
    // // public function addapt(){
    // //      $title = array(
    // //         'title' => "Appointments"
    // //     );
    
    // //     $fetchDat = array(
    // //         'allappoint' => $this->MyLcp_Model->fetchAll("clinic_tbl"),
    // //     );
    // //     $arrMerge = array_merge($title, $fetchDat);
    // //     $this->load->view("portal/includes/header", $arrMerge);
    // //     $this->load->view("portal/includes/navbar");
    // //     $this->load->view("portal/appointment");
    // //     $this->load->view("portal/includes/footer");
    // //     $sbmt = $this->input->post("sreg");
    // //     if (isset($sbmt)) {
    // //         date_default_timezone_set("Asia/Manila");
    // //         $sbmtData = array(
    // //             'Name' => $this->input->post("Name"),
    // //             'contact_num' => $this->input->post("contact_num"),
    // //             'Sex' => strtotime($this->input->post("Sex")),
    // //             'Specialization' => $this->input->post("Specialization"),
    // //             'Description' => $this->input->post("Description"),
    // //             'Assess' => $this->input->post("Assess"),
    // //             'app_time' => strtotime($this->input->post("app_time")),
    // //             'date_created' => time(),

    // //             // 'position' => $this->input->post("actype"),
    // //             // 'sendfrom' => $this->input->post("sfrom"),
    // //             // 'email' => $this->input->post("sappemail"),
    // //             // 'subject' => $this->input->post("toSubj"),
    // //             // 'body_content' => $this->input->post("sbcontent"),
    // //             // 'appointment_time' => strtotime($this->input->post("abdate")),
    // //             // 'date_created' => time(),
    // //         );
    // //         $this->MyLcp_Model->add_event($sbmtData);
    // //         redirect('portal/addapt');
    // //     }
    // //  }
    // public function addapt(){
    //     $data = array(
    //         'specs' => $this->MyLcp_Model->fetchAllDoc1() 
    //     );
    //     $this->load->view("portal/includes/header");
    //     $this->load->view("portal/includes/navbar");
    //     $this->load->view("portal/search",$data);
    //     $this->load->view("portal/includes/footer");  

    // }
    // public function search1() {
    //     $keyword=$this->input->post('specialization');
    //     $data = $this->MyLcp_Model->search($keyword); 
        
    //     // $accountDetails = $this->MyLcp_Model->getinfo("doc_tbl", $data); 
    //     // $accountDetails = $accountDetails[0];
    //     // $data1 = $accountDetails->doc_id; 
       
    //     $data1 = array(
    //         'users' => $this->MyLcp_Model->join($keyword) 
    //     );
    //     // $this->load->view('portal/includes/header_search');    
    //     $this->load->view('portal/search_sub',$data1); 
    //     // $this->load->view('portal/includes/footer_search'); 
       
      
    // }
   
}
