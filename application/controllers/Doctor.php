<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Doctor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','session','form_validation'));
        
    //     if($this->session->has_userdata('isloggedin') == FALSE ||
    //     $this->session->userdata('user_type') != 0){
    //         redirect('dashboard');
    // }

}
public function index() {
        $title = array(
           'title' => "Home",
       );
        $dataArr = $this->session->dataadmin;
        $arrMerge = array_merge($title,$dataArr);
        $this->load->view("doctor/includes/header",$arrMerge);
        $this->load->view("doctor/dashboard");
        $this->load->view("doctor/includes/footer");

    } 
    public function dashboard() {
        $this->load->view("doctor/includes/header");
        $this->load->view("doctor/includes/navbar");
        $this->load->view("doctor/dashboard");
        $this->load->view("doctor/includes/footer");

    } 
public function login(){
    $login_cred = array(
        'doctor_uname' => $this->input->post('doctor_uname'),
        'doctor_pass'  => $this->input->post('doctor_pass'),
    );

    $user = $this->MyLcp_Model->fetch('doc_tbl',$login_cred);
    if(!$user): //if not existing user
        $this->session->set_flashdata('exist',1);
        redirect('doctor/dashboard'); // TO CHANGE!
    else: //else validate account
        $user = $user[0];
        if(!$user->active):
            $this->session->set_flashdata('ban',1);
            redirect('doctor/dashboard'); // TO CHANGE!!
        else:
            $this->session->set_userdata(array(
                'doc_id'=> $user->doc_id,
                'doc_utype'=> $user->doc_utype,
                'isloggedin' => TRUE,
            ));

            redirect('doctor/dashboard');// TO CHANGE!
        endif;
    endif;
}
    public function cliniclogs(){
    
    

        $doctor = array(
            "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
            "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
        );
        $this->load->view("doctor/includes/header");
        $this->load->view("doctor/includes/navbar");
        $this->load->view("doctor/cliniclogs");
        $this->load->view("doctor/includes/footer");
    } 
    public function editprofile(){
    $id = $this->uri->segment(3);
    $data = array(
        "doc_id" => $id
    );
    $doc_acc = $this->MyLcp_Model->getDocinfo('doc_tbl', array('doc_id'=>$this->session->userdata('doc_id')));
    $sbmt = $this->input->post("sreg");
    if(isset($sbmt)){
        $updateData = array(
            'specialty'    => $this->input->post("specialty")
        );
        $this->MyLcp_Model->editdocprofile($updateData);
        redirect("doctor/profile",$updateData);
    }
}  
    public function appointments(){
     $fetchappt = $this->MyLcp_Model->fetchpatientappt();
     $fetch_appointment['appointments'] = $fetchappt;

     $this->load->view("doctor/includes/header");
     $this->load->view("doctor/includes/navbar");    
     $this->load->view("doctor/appointments",$fetch_appointment);
     $this->load->view("doctor/includes/footer");
    
    }

     public function profile(){
          $doctor = $this->MyLcp_Model->getDocInfo();

        $doctor_profile['profiles'] = $doctor;
      
        $this->load->view("doctor/includes/header");
        $this->load->view("doctor/includes/navbar");
        $this->load->view("doctor/profile", $doctor_profile);
        $this->load->view("doctor/includes/footer");
     }
     public function patientrec(){
            $fetchPatientRec = $this->MyLcp_Model->fetchDiagnosis();
            $fetch_patientRec['patients'] = $fetchPatientRec;
         
         $this->load->view("doctor/includes/header");
         $this->load->view("doctor/includes/navbar");
         $this->load->view("doctor/patientrec", $fetch_patientRec);
         $this->load->view("doctor/includes/footer");
            $doctor = array(
            "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
            "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
            "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
        );
    }
    public function patientdetails(){

       $fetchPatient = $this->MyLcp_Model->fetch('patient_tbl');
       $fetch_patient['patients'] = $fetchPatient;
       $this->load->view("doctor/includes/header");
       $this->load->view("doctor/includes/navbar");
       $this->load->view("doctor/patientdetails",$fetch_patient);
       $this->load->view("doctor/includes/footer");
    //   $doctor = array(
    //         "clinic" => $this->MyLcp_Model->fetchDocClinic('clinic_tbl', array('clinic_id'=>$this->session->userdata('clinic_id'))),
    //         "doctor" => $this->MyLcp_Model->fetchDoc($this->session->userdata('clinic_id')),
    //         "alldoc" => $this->MyLcp_Model->fetchAllDoc(),
    //     );
      $title = array(
            'title' => "Add new Patient"
        );      
        
        $this->form_validation->set_rules('fname',"Firstname","required|alpha"); 
        $this->form_validation->set_rules('mname',"Middlename","required|alpha");   
        $this->form_validation->set_rules('lname',"Lastname","required|alpha");  
        $this->form_validation->set_rules('age',"Age","required|numeric");
        $this->form_validation->set_rules('bdate',"Birthdate","required");    
        $this->form_validation->set_rules('street',"Address","required");    

        $this->form_validation->set_message('required',"{field} is required ");
        $this->form_validation->set_message('number',"{field} must be a numeric ");
        $this->form_validation->set_message('min_length',"{field} must be exactly 11 numbers ");
        $this->form_validation->set_message('max_length',"{field} must be exactly 11 numbers ");
        //$finame = $this->input->post('fname');
        //$laname = $this->input->post('lname');
        $sbmt = $this->input->post("subAddPat");
        if (isset($sbmt)){
            date_default_timezone_set("Asia/Manila");
            
             $data = array(
                'fname' => $this->input->post('fname'),
                'mname' => $this->input->post('mname'),
                'lname' => $this->input->post('lname'),
                'sex' => $this->input->post('sex'),
                'bdate' => strtotime($this->input->post('bdate')),
                'age' => $this->input->post('age'),
                'street' => $this->input->post('street'),
                'con_num' => $this->input->post('con_num'),
                'email' => $this->input->post('email'),
                  
            );
            $data1 = array(
                'diagname' => $this->input->post('diagname'),
                'height' => $this->input->post('height'),
                'weight' => $this->input->post('weight'),
                'blood_press' => $this->input->post('blood_press'),
                'treatment' => ($this->input->post('treatment')),
                'pres_med' => $this->input->post('pres_med'),
            );

             $this->MyLcp_Model->insertData('patient_tbl',$data );
             $this->MyLcp_Model->insertData('diagnosis_tbl',$data1);
                echo "<script>alert('Added Successfully');"
            . "window.location='". base_url()."doctor/patientdetails'</script>";
            // if(!$this->MyLcp_Model->insertData('patient_tbl',$data)){
            //     echo"error";
            // }else{
            //    $this->load->view("doctor/includes/header");
            //    $this->load->view("doctor/includes/navbar");
            //    $this->load->view("doctor/patientdetails");
            //    $this->load->view("doctor/includes/footer");
            // }
          }

        }   

    public function genpat_tbl(){
     
     $this->load->library('Pdf');
     $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('https://www.roytuts.com');
	$pdf->SetTitle('Patients Directory');
	$pdf->SetSubject('Report generated using Codeigniter and TCPDF');
	$pdf->SetKeywords('TCPDF, PDF, MySQL, Codeigniter');

	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont('times', 'BI', 12);
	
	// ---------------------------------------------------------
	
	
	//Generate HTML table data from MySQL - start
	$template = array(
		'table_open' => '<table border="1" cellpadding="2" cellspacing="1">'
	);

	$this->table->set_template($template);

	$this->table->set_heading('Patient_ID','First Name', 'Middle Name', 'Last Name', 'Age', 'Birthday','Sex', 'Contact Number','Street','Email');
	
	$Patient = $this->MyLcp_Model->fetchPatientTbl();
		
	foreach ($Patient as $pat):
		$this->table->add_row($pat->patient_id, $pat->fname, $pat->mname, $pat->lname, $pat->age, $pat->bdate, $pat->sex, $pat->con_num ,$pat->street, $pat->email);
	endforeach;
	
	$html = $this->table->generate();
	//Generate HTML table data from MySQL - end
	
	// add a page
	$pdf->AddPage();
	
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');
	
	// reset pointer to the last page
	$pdf->lastPage();

	//Close and output PDF document
	$pdf->Output(md5(time()).'.pdf', 'D');
}

   public function gendiag_tbl(){
     
     $this->load->library('Pdf');
     $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('https://www.roytuts.com');
	$pdf->SetTitle('Patients Directory');
	$pdf->SetSubject('Report generated using Codeigniter and TCPDF');
	$pdf->SetKeywords('TCPDF, PDF, MySQL, Codeigniter');

	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont('times', 'BI', 12);
	
	// ---------------------------------------------------------
	
	
	//Generate HTML table data from MySQL - start
	$template = array(
		'table_open' => '<table border="1" cellpadding="2" cellspacing="1">'
	);

	$this->table->set_template($template);

	$this->table->set_heading('Patient_ID','First Name', 'Middle Name', 'Last Name', 'Diagnosis','Height', 'Weight','Blood Pressure','Treatment', 'Prescribed Medicine','Date Created');
	
	$Patient = $this->MyLcp_Model->fetchDiagnosisTbl();
		
	foreach ($Patient as $pat):
		$this->table->add_row($pat->patient_id, $pat->fname, $pat->mname, $pat->lname, $pat->diagname, $pat->height, $pat->weight, $pat->blood_press ,$pat->treatment, $pat->pres_med);
	endforeach;
	
	$html = $this->table->generate();
	//Generate HTML table data from MySQL - end
	
	// add a page
	$pdf->AddPage();
	
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');
	
	// reset pointer to the last page
	$pdf->lastPage();

	//Close and output PDF document
	$pdf->Output(md5(time()).'.pdf', 'D');
    }
    
    public function logout(){
        $this->session->session_destroy();
        redirect('portal');
    }

}