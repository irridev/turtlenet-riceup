<?php
class Dashboard extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','form_validation'));
        
        if($this->session->has_userdata('isloggedin')==FALSE){
            redirect('portal');
        }
    }
    public function index(){
        if($this->session->userdata('user_type')==0):
            redirect('admin/dashboard');
        elseif($this->session->userdata('user_type')==1):
            redirect('clinic/dashboard');
        elseif($this->session->userdata('user_type')==2):
            redirect('doctor/index');
        elseif($this->session->userdata('user_type')==3):
            redirect('portal/appointment');
        elseif($this->session->userdata('user_type')==4):
            redirect('moderator/dashboard');
        else:
            redirect('dashboard/logout');
        endif;
    }

    public function logout(){
        session_destroy();
        
    }
    
    public function clinic(){

    }
    public function doctor(){

    }
    public function userpatient(){

    }
}
    