<?php
class Admin extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->library(array('session'));
        // if($this->session->has_userdata('isAdminloggedin') == TRUE){
        //     redirect('admin'); 
        // }
        // else{
        //     redirect('view');
        // } 
    }
    public function index() {
        $this->load->view("admin_login");
    }
    public function adminlogin_submit() {
        $data = array(
            'username' => $this->input->post('a_username'),
            'password' => sha1($this->input->post('a_password')),
        );
        $accountDetails = $this->MyLcp_Model->getinfo("admin_tbl", $data);
        if (!$accountDetails) {
            echo "<script>alert('No results found');"
            . "window.location='". base_url()."adminlogin'</script>";
        } else {
            $this->session->isAdminloggedin = true;
            $this->session->admin_id = $accountDetails->admin_id;
            redirect('admin/view'); 
        }
    }
    public function view() {
        $this->load->view("admin_view");
    }
    public function edit() {
        $data = array(
            'post' => $this->MyLcp_Model->fetchAllPost(),
        );
        $this->load->view("admin_view_edit",$data);
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('admin');
    }
    public function delete_account() {
        $id = $this->uri->segment(3);
        $this->lcp_model->deletepost($id);
        redirect('admin/edit');
    }
    public function submit(){
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('pic')) {
            $image = $this->upload->data('file_name');
        }
        
           
        $data = array(
            'category' => $this->input->post('category'),
            'title' => $this->input->post('title'),
            'context' => $this->input->post('message'),
            'end_date' => $this->input->post('end_date'),
            'create_date' => time(),
            'image' => $image,
        );
        $this->MyLcp_Model->insertData('post_tbl',$data );
        echo "<script>
            alert('Successfully Posted');
            window.location.href='" . base_url() . "admin/view';
        </script>";
    }
}