<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('MyLcp_Model');
        $this->load->helper(array('form','string'));
        $this->load->library(array('email','session','form_validation'));
        
        if($this->session->has_userdata('isClinicloggedin') == TRUE){
            redirect('clinic/dashboard'); 
        }
        else if($this->session->has_userdata('isDocloggedin') == TRUE){
            redirect('doctor/dashboard'); 
        }
        else if($this->session->has_userdata('isPatientloggedin') == TRUE){
            redirect('patient/dashboard'); 
        }
    }

    public function index(){
        $this->load->view('landing/login');
    }
    public function login() {
        $data = array(
            'username' => $this->input->post('user'),
            'password' => $this->input->post('pass'),
        );
        $accountDetails = $this->MyLcp_Model->getinfo("user_tbl", $data);
        if (!$accountDetails) {
            echo "<script>alert('No results found');"
            . "window.location='". base_url()."landing'</script>";
        } 
        else {
            $accountDetails = $accountDetails[0];
            if($accountDetails->user_type == 1){
                $this->session->isClinicloggedin = TRUE;
                $this->session->userid = $accountDetails->user_id;
                $id = $this->session->userid;
         
                $data = array(
                    'user_id' => $id,
                );
                $accountDetails = $this->MyLcp_Model->getinfo("user_tbl", $data); 
                $accountDetails = $accountDetails[0];
                
                // $data1 = array(
                //     'id' => $accountDetails->user_id,
                //     'firstname' => $accountDetails->firstname,
                //     'lastname' => $accountDetails->lastname,
                //     'description' => 'Login @ ',
                //     'time' => time(), 
                // );
                                
                // $this->lcp_model->insert('user_logs',$data1 );
                redirect('clinic/dashboard');
            }
            else if ($accountDetails->user_type == 2) {
                $this->session->isDocloggedin = TRUE;
                $this->session->userid = $accountDetails->user_id;
                $id = $this->session->userid;
         
                $data = array(
                    'user_id' => $id,
                );
                $accountDetails = $this->MyLcp_Model->getinfo("user_tbl", $data); 
                $accountDetails = $accountDetails[0];
                
                // $data1 = array(
                //     'id' => $accountDetails->user_id,
                //     'firstname' => $accountDetails->firstname,
                //     'lastname' => $accountDetails->lastname,
                //     'description' => 'Login @ ',
                //     'time' => time(), 
                // );
                                
                // $this->lcp_model->insert('user_logs',$data1 );
                redirect('doctor/dashboard');
            }
            else if ($accountDetails->user_type == 3) {
                $this->session->isPatientloggedin = TRUE;
                $this->session->userid = $accountDetails->user_id;
                $id = $this->session->userid;
         
                $data = array(
                    'user_id' => $id,
                );
                $accountDetails = $this->lcp_model->MyLcp_Model("user_tbl", $data); 
                $accountDetails = $accountDetails[0];
                
                // $data1 = array(
                //     'id' => $accountDetails->user_id,
                //     'firstname' => $accountDetails->firstname,
                //     'lastname' => $accountDetails->lastname,
                //     'description' => 'Login @ ',
                //     'time' => time(), 
                // );
                                
                // $this->lcp_model->insert('user_logs',$data1 );
                redirect('patient/dashboard');
            }
            else if ($accountDetails->user_type == 4) {
                echo "<script>alert('Your Account is Blocked');"
                . "window.location='". base_url()."landing'</script>";
            }  
        }
    } 
}

    // public function register() {
    //     $this->form_validation->set_rules('g-recaptcha-response', "CAPTCHA", "required|callback_checkrecaptcha");

    //     if (!$this->form_validation->run()){
    //         $captcha = array(
    //             'widget' => $this->recaptcha->getWidget(),
    //             'myscript' => $this->recaptcha->getScriptTag()
    //         );
    //         $this->load->view('landing/register',$captcha);
    //     }
    // }

    // public function regsub(){    
    //     $v = random_string('alnum',15);
    //     $user = array(
    //         'user_fname' => $this->input->post('fname'),
    //         'user_lname' => $this->input->post('lname'),
    //         'user_uname' => $this->input->post('uname'),
    //         'user_pass' => $this->input->post('pass'),
    //         'user_email' => $this->input->post('email'),
    //         'user_type' => 3,
    //         'reg_at' => time(),
    //         'active' => 0,
    //         'vcode' => $v
    //     );

    //     $ev1 = array('user_email'=>$this->input->post('email'));
    //     $ev2 = array('email'=>$this->input->post('email'));
    //     $verifyuser = $this->lcp_model->fetch('user_tbl',$ev1);
    //     $verifypat = $this->lcp_model->fetch('patient_tbl',$ev2);

    //     if($verifyuser||$verifypat): // check if existing user
    //         $this->session->set_flashdata('exist',1);
    //         redirect('landing/register');
    //     else: // else insert to database
    //         if(!$this->lcp_model->insertData('user_tbl',$user)): // if insert fails
    //             $this->session->set_flashdata('fail',1);
    //             redirect('landing/register');
    //         else: // else send email verification
    //             $data = array(
    //                 "title" => "Registration Almost Complete!",
    //                 "content" => "To complete the registration process, please verify your account by clicking the link below:",
    //                 "func" => "verify",
    //                 "code" => $v
    //             );

    //             $this->email->from('arthom0409@gmail.com', 'Admin');
    //             $this->email->to($this->input->post('email'));
    //             $this->email->subject('Email Verification');
    //             $this->email->message($this->load->view('mail',$user,true));
                
                
    //             if ($this->email->send()): // if send email sucess
    //                 $this->session->set_flashdata('done',1);
    //                 redirect("landing");
    //             else: // else remain in register page
    //                 $this->email->print_debugger();
    //                 $this->session->set_flashdata('fail',1);
    //                 redirect("landing/register");
    //             endif;
    //         endif;
    //     endif;
    // }
    
    
    // public function checkrecaptcha($response){
    //     if(!empty($response)){
    //         $response = $this->recaptcha->verifyResponse($response);
    //         if($response['success'] === TRUE){
    //             return true;
    //         }else{
    //             return false;
    //         }
    //     }
    // }
    
    // public function verify() {
    //     $data = array("verify"=>1);
    //     $this->lcp_model->updateData('user_tbl',$data,array('vcode'=>$this->uri->segment(3)));
    //     $this->session->set_flashdata('verified',1);
    //     redirect("portal");
    // }
    
    // public function forgot() {
    //     $this->lcp_validation->set_rules('email',"Email","required|valid_email");
        
    //     if ($this->form_validation->run() == FALSE){
    //         $this->load->view('forgot_form');
    //     } else {
    //         $v = random_string('alnum',15);
    //         $ev1 = array('doc_email'=>$this->input->post('email'));
    //         $ev2 = array('patient_email'=>$this->input->post('email'));
    //         $verifydoc = $this->lcp_model->fetch('doc_tbl',$ev1);
    //         $verifypat = $this->lcp_model->fetch('patient_tbl',$ev2);
            
    //         if(!$verifydoc||!$verifypat){ // check if existing email
    //             $this->session->set_flashdata('exist',1);
    //             redirect("landing/forgot");
    //         } else { // else insert to database
    //             if(!$this->lcp_model->updateData('doc_tbl',array("vcode"=>$v),$ev1)||
    //                     !$this->lcp_model->updateData('patient_tbl',array("vcode"=>$v),$ev2)){ // if update fails
    //                 $this->session->set_flashdata('fail',1);
    //                 redirect("landing/forgot");
    //             } else{ // else send email verification
    //                 $code = array(
    //                     "title" => "Password Reset",
    //                     "content" => "To reset your password, please click the link below:",
    //                     "code" => $v
    //                 );

    //                 $this->email->from('arthom0409@gmail.com', 'Admin');
    //                 $this->email->to($this->input->post('email'));
    //                 $this->email->subject('Password Reset');
    //                 $this->email->message($this->load->view('mail',$code,true));

    //                 $result = $this->email->send();
    //                 //echo $result;
                    
    //                 if ($result){ // if send email success
    //                     $this->session->set_flashdata('done',1);
    //                     redirect("portal");
    //                 } else{ // else remain in register page
    //                     $this->session->set_flashdata('fail',1);
    //                     redirect("landing/forgot");
    //                 }
    //             }
    //         }
    //     }
    // }
    
    // public function reset() {
    //     $this->form_validation->set_rules('new_pass',"Password","required|min_length[8]");
    //     $this->form_validation->set_rules('vnew_pass',"Verify Password","required|matches[new_pass]");
        
    //     if($this->form_validation->run() == FALSE):
    //         $this->load->view('reset_form',array("code"=>$this->uri->segment(3)));
    //     else:
    //         $data = array('user_pass'=>$this->input->post('new_pass'));
    //         $this->lcp_model->updateData('user_tbl',$data,array('vcode'=>$this->input->post('code')));
    //         $this->session->set_flashdata('changed',1);
    //         redirect('landing');
    //     endif;
    // }


